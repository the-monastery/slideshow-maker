package com.cbc.ourgame.slideshowmaker.components.ui
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class PressButton extends FrameButton
	{
		private var _callBack:Function;
		
		public function PressButton(view:MovieClip)
		{
			super(view);
			enable();
		}
		
		override public function addCallback(callback:Function):void
		{
			_callBack = callback;
		}
		
		override public function disable(showAlpha:Boolean = false, showGrey:Boolean = false):void
		{
			super.disable();
			view.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}
		
		override public function enable():void
		{
			super.enable();
			view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			view.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			view.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function onMouseUp(e:MouseEvent):void
		{
			view.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
			view.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function onEnterFrame(e:Event):void
		{
			_callBack(e);
		}
		
	}
}