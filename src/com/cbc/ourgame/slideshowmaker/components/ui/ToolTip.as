package com.cbc.ourgame.slideshowmaker.components.ui
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.shapes.EasyRectangle;
	import com.ghostmonk.text.EmbeddedText;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;

	public class ToolTip extends EasyRectangle
	{
		private var _bitmapText:Bitmap;
		
		public function ToolTip()
		{
			super(0xFFFFCC, 1);
			_bitmapText = new Bitmap();
		}
		
		public function set text(value:String):void
		{
			var text:EmbeddedText = new EmbeddedText("Arial", 10, 0x000000, true);	
			text.text = value;
			_bitmapText.bitmapData = new BitmapData(text.width, text.height, true, 0);
			_bitmapText.bitmapData.draw(text);
			create(text.width, text.height);
			addChild(_bitmapText);
		}
		
		public function show():void
		{
			alpha = 0;
			Tweener.addTween(this, {alpha:1, time:0.3, transition:Equations.easeNone});
		}
		
		public function hide():void
		{
			Tweener.addTween(this, {alpha:0, time:0.3, transition:Equations.easeNone, onComplete:remove});
		}
		
		private function remove():void
		{
			try
			{
				parent.removeChild(this);
			}	
			catch(e:Error)
			{
			}
		}
		
	}
}