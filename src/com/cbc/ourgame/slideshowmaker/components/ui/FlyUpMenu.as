package com.cbc.ourgame.slideshowmaker.components.ui
{
	import assets.audiochooser.FlyUpMenuAsset;
	
	import com.cbc.ourgame.slideshowmaker.components.audio.MenuItem;
	import com.ghostmonk.shapes.InnerShadowRectangle;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class FlyUpMenu
	{
		private var _view:FlyUpMenuAsset;
		private var _bgFill:Sprite;
		private var _menuItems:Array;
		private var _config:XML;
		private var _mask:InnerShadowRectangle;
		private var _menuItemsHolder:Sprite;
		private var _currentSelection:int;
		private var _callBack:Function;
		
		private var _originY:Number;
		
		public function get view():FlyUpMenuAsset 	{ return _view; }
		public function get currentSelection():int 	{ return _currentSelection; }
		public function get menuItems():Array		{ return _menuItems; }
		
		public function FlyUpMenu(view:FlyUpMenuAsset, config:XML)
		{
			_view = view;
			_originY = _view.background.y;
			_config = config;
			
			_view.selector.addEventListener(MouseEvent.CLICK, onClick);
			_view.selector.addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			
			_view.selector.buttonMode = true;
			
			_view.addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			_view.selector.gotoAndStop(1);
			_bgFill = new Sprite();
			_view.addChildAt(_bgFill, _view.getChildIndex(_view.background) + 1);
			
			fillBackground(_view.background.width, _view.background.height);
			
			_mask = new InnerShadowRectangle(0,_view.width, _view.height);
			_view.addChild(_mask);
			_view.mask = _mask;
			
			_currentSelection = 0;
		}
		
		public function addClickCall(callback:Function):void
		{
			_callBack = callback;
			createItems(_config.track, onMenuItemClick);
			_config = null;
		}
		
		private function onMenuItemClick(menuItem:MenuItem):void
		{
			_callBack(menuItem.request, menuItem.id);
			_currentSelection = _menuItems.indexOf(menuItem);
			onRollOut(null);
		}
		
		private function onClick(e:MouseEvent):void
		{
			_view.background.y = _view.background.y - _view.background.height*(_menuItems.length-1);
			_view.background.scaleY = _menuItems.length;
			fillBackground(_view.background.width, _view.background.height);
			
			_mask.height = _view.background.height;
			_mask.y = _bgFill.y = _menuItemsHolder.y = _view.background.y;
			
			for each(var item:MenuItem in _menuItems)
				item.enable();
		}
		
		private function onRollOver(e:MouseEvent):void
		{	
			_view.selector.gotoAndStop(2);
		}
		
		private function onRollOut(e:Event):void
		{
			_view.background.y = _originY;//_view.background.y + _view.background.height/(_menuItems.length) * (_menuItems.length-1);
			_view.background.scaleY = 1;
			_view.selector.gotoAndStop(1);

			fillBackground(_view.background.width, _view.background.height);
			
			_mask.height = _view.background.height;
			
			_mask.y = _bgFill.y = _view.background.y;
			_menuItemsHolder.y = _view.background.y - (_currentSelection*(_menuItems[0].height + 1));
			
			for each(var item:MenuItem in _menuItems)
				item.disable();
		}
		
		private function fillBackground(width:Number, height:Number):void
		{
			_bgFill.graphics.clear();
			_bgFill.graphics.beginBitmapFill(new AudioSelectGrid(0,0));
			_bgFill.graphics.drawRect(1,2,width-3,height-3);
			_bgFill.graphics.endFill();
			_bgFill.width = width - 3;
			_bgFill.height = height;
		}
		
		private function createItems(items:XMLList, callback:Function):void
		{
			_menuItemsHolder = new Sprite();
			_menuItems = [];
			for(var i:int = 0; i<items.length(); i++)
			{
				var selector:MenuItem = new MenuItem(items[i], callback);
				selector.y = (selector.height + 1)*i + 2;
				selector.x = 2;
				_menuItems.push(selector);
				_menuItemsHolder.addChild(selector);
				selector.disable();
			}
			_view.addChildAt(_menuItemsHolder,2);
		}

	}
}