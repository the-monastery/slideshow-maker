package com.cbc.ourgame.slideshowmaker.components
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.system.Security;

	public class DartAd extends Sprite
	{
		private var _loader:Loader;
		private var _callback:Function;
		private var _errorCallBack:Function;
		
		private const AD:String = "images/trailer.jpg";
		
		public function DartAd(callBack:Function, errorCallBack:Function)
		{
			_callback = callBack;
			_errorCallBack = errorCallBack;
			_loader = new Loader();
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
			
			//Security.loadPolicyFile("http://m1.2mdn.net/crossdomain.xml");
			
			_loader.load(new URLRequest(AD), new LoaderContext(true));
		}
		
		private function onComplete(e:Event):void
		{
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
			_loader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			//_callback(_loader.content, LINK);
		}
		
		private function onError(e:IOErrorEvent):void
		{
			_errorCallBack();
		}
		
	}
}