package com.cbc.ourgame.slideshowmaker.components.editor
{
	import assets.galleryscreen.GalleryChooserAsset;
	import assets.imagepreview.PreviewPanelAsset;
	import assets.library.ImageLibraryAsset;
	
	import com.cbc.ourgame.slideshowmaker.components.thumblib.Library;
	import com.cbc.ourgame.slideshowmaker.components.ui.FrameButton;
	import com.cbc.ourgame.slideshowmaker.events.GalleryViewEvent;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.GalleryData;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	[Event (name="addGallery", type="com.cbc.ourgame.slideshowmaker.events.GalleryDataEvent")]
	[Event (name="galleryChooser", type="com.cbc.ourgame.slideshowmaker.events.GalleryViewEvent")]
	[Event (name="galleryEditor", type="com.cbc.ourgame.slideshowmaker.events.GalleryViewEvent")]
	
	public class GalleryEditor extends EventDispatcher
	{	
		private var _parent:DisplayObjectContainer;
		private var _library:Library;
		private var _previewPanel:ImagePreview;
		private var _galleryChooser:GalleryChooser;
		private var _back:FrameButton;
		
		public var _currentGalleryTitle:String;
		
		private var _chooserMode:Boolean;
		
		public function get library():Library { return _library; }
		public function get previewPanel():ImagePreview { return _previewPanel; }
		public function get galleryChooser():GalleryChooser { return _galleryChooser; }
		public function get currentGalleryTitle():String { return _currentGalleryTitle; }
		
		public function GalleryEditor(config:XML, library:ImageLibraryAsset, previewPanel:PreviewPanelAsset, backBtn:MovieClip, galleryChooser:GalleryChooserAsset)
		{
			_parent = backBtn.parent;
			_back = new FrameButton(backBtn);
			_back.addCallback(showChooser);
			_library = new Library(library, config.brokenImages.@small);
			_previewPanel = new ImagePreview(previewPanel, config.brokenImages.@large);
			_galleryChooser = new GalleryChooser(galleryChooser, galleryClick);
			_galleryChooser.countPostfix = config.countMsg.@en;
			
			showChooser();
		}
		
		public function show():void
		{
			if(_chooserMode) 
				_parent.addChildAt(_galleryChooser.view, 3);
			else 
			{
				_parent.addChildAt(_back.view, 3);
				_parent.addChildAt(_library.view, 3);
				_parent.addChildAt(_previewPanel.view, 3);
				
			}
		}
		
		public function hide():void
		{
			if(_chooserMode)
				_parent.removeChild(_galleryChooser.view);
			else
			{
				_parent.removeChild(_library.view);
				_parent.removeChild(_previewPanel.view);
				_parent.removeChild(_back.view);
			}
		}
		
		private function showEditor():void
		{
			_chooserMode = false;
			_parent.removeChild(_galleryChooser.view);
			_parent.addChildAt(_back.view, _parent.numChildren - 1);
			_parent.addChildAt(_library.view, 3);
			_parent.addChildAt(_previewPanel.view, 3);
			dispatchEvent(new GalleryViewEvent(GalleryViewEvent.GALLERY_EDITOR));
		}
		
		private function showChooser(e:MouseEvent = null):void
		{
			_chooserMode = true;
			_parent.addChildAt(_galleryChooser.view, 3);
			_parent.removeChild(_back.view);
			_parent.removeChild(_library.view);
			_parent.removeChild(_previewPanel.view);
			dispatchEvent(new GalleryViewEvent(GalleryViewEvent.GALLERY_CHOOSER));
		}
		
		private function galleryClick(data:GalleryData):void
		{
			_currentGalleryTitle = data.title;
			showEditor();
			library.createGallery(data.photos);
		}
		
	}
}