package com.cbc.ourgame.slideshowmaker.components.editor
{
	import assets.galleryscreen.GalleryItemAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.cbc.ourgame.slideshowmaker.AppFacade;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.GalleryData;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.PhotoData;
	import com.ghostmonk.net.SimpleLoader;
	
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;

	public class GalleryItem extends GalleryItemAsset
	{
		private var _loader:SimpleLoader;
		private var _callBack:Function;
		private var _galleryData:GalleryData;
		private const _shadow:DropShadowFilter = new DropShadowFilter(2,45,0x999999,1,1,1,1,1);
		
		public function GalleryItem(data:GalleryData, callBack:Function, countMsg:String)
		{
			_loader = new SimpleLoader(thumbLoaded, onThumbError);
			_callBack = callBack;
			_galleryData = data;
			title.htmlText = "<b>"+data.title+"</b>";
			description.htmlText = data.description;
			count.htmlText = data.photos.length+ " "+countMsg;
			//meta.htmlText = data.tags;		
			_loader.load(new URLRequest(PhotoData(data.photos[data.photos.length-1]).small), new LoaderContext(true));	
			buttonMode = true;
			mouseChildren = false;
			addEventListener(MouseEvent.CLICK, onClick);
			addEventListener(MouseEvent.ROLL_OVER, onRollover);
			addEventListener(MouseEvent.ROLL_OUT, onRollout);
			
			outline.alpha = 0;
			thumb.actionState.alpha = 0;
			background.filters = [_shadow];
		}
		
		private function onThumbError():void
		{
			var loader:SimpleLoader = new SimpleLoader(thumbLoaded);
			loader.load(new URLRequest(AppFacade.SMALL_BROKEN_IMG));
		}
		
		private function thumbLoaded(bitmap:Bitmap):void
		{
			thumb.thumbHolder.addChild(bitmap);
			thumb.removeChild(thumb.loadIndicator);
		}
		
		private function onClick(e:MouseEvent):void
		{
			_callBack(_galleryData);
		}
		
		private function onRollout(e:MouseEvent):void
		{
			background.filters = [_shadow];
			Tweener.addTween(outline, {alpha:0, time:0.3, transition:Equations.easeNone});
		}
		
		private function onRollover(e:MouseEvent):void
		{
			background.filters = [];
			Tweener.addTween(outline, {alpha:1, time:0.3, transition:Equations.easeNone});
		}
		
	}
}