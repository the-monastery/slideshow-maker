/**
 * Written By: Nicholas Hillier, Jan 16, 2009
 * This is used the panel to preview an image from the thumbnail library populated by Pluck
 * It contains an image loader to load a larger version of the chosen image, and text fields
 * to display metadata of the chosen image.
 */
package com.cbc.ourgame.slideshowmaker.components.editor
{
	import assets.imagepreview.PreviewPanelAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.cbc.ourgame.slideshowmaker.components.ui.FrameButton;
	import com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.TimelineData;
	import com.ghostmonk.net.ImageLoader;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.text.TextField;

	[Event (name="previewPanClick", type="com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent")]

	public class ImagePreview extends EventDispatcher
	{
		private var _timelineData:TimelineData;
		private var _view:PreviewPanelAsset;
		private var _loader:ImageLoader;
		private var _addToTimeline:FrameButton;
		private var _title:TextField;
		private var _description:TextField;
		private var _location:TextField;
		private var _loadIndicator:MovieClip;
		private var _scaleWidth:Number;
		private var _scaleHeight:Number;
		
		public function get view():PreviewPanelAsset
		{
			return _view;
		}
		
		/**
		 * 
		 * @param view - Asset from the SWC
		 * 
		 */
		public function ImagePreview(view:PreviewPanelAsset, brokenImgURL:String)
		{
			_view = view;
			_loader = new ImageLoader(true);
			_loader.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_loader.buttonMode = true;
			
			_addToTimeline = new FrameButton(view.addImageBtn);
			_addToTimeline.addCallback(addToTimeline);
			
			//For now remove the add to timeline button
			_view.removeChild(_addToTimeline.view);
			
			_title = view.picTitle;
			_description = view.picDescription;
			_location = view.picLocation;
			_loadIndicator = view.preloader;
			view.removeChild(_loadIndicator);
			_loadIndicator.alpha = 0;
			view.imagePreviewHolder.addChild(_loader);
			
			_scaleWidth = view.imagePreviewHolder.width;
			_scaleHeight = view.imagePreviewHolder.height;
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			dispatchEvent(new TimelineDataEvent(TimelineDataEvent.LIBRARY_DRAG, _timelineData));
		}
		
		public function addPreview(data:TimelineData):void
		{
			if(data != _timelineData)
			{
				_timelineData = data;
				_title.htmlText = "<b>"+data.photoData.title+"</b>";
				_description.htmlText = data.photoData.description;
				//_location.htmlText = data.photoData.tags;
				/* Tweener.addTween(_title, {_text:data.photoData.title, time:0.3, transition:Equations.easeNone});
				Tweener.addTween(_description, {_text:data.photoData.description, time:0.3, transition:Equations.easeNone});
				Tweener.addTween(_location, {_text:data.photoData.tags, time:0.3, transition:Equations.easeNone}); */
				view.addChild(_loadIndicator);
				Tweener.addTween(_loadIndicator, {alpha:1, time:0.3, transition:Equations.easeNone});
				_loader.alpha = 0;
				//Tweener.addTween(_loader, {alpha:0, time:0.2, transition:Equations.easeNone});
				_loader.load(data.photoData.medium, imageLoaded);
			}
		}
		
		public function addToTimeline(e:MouseEvent):void
		{
			dispatchEvent(new TimelineDataEvent(TimelineDataEvent.PREVIEW_PANE_CLICK, _timelineData));
		}
		
		private function imageLoaded(bitmap:Bitmap):void
		{  
			bitmap.scaleX = bitmap.scaleY = Math.min(_scaleWidth/bitmap.width, _scaleHeight/bitmap.height);
			bitmap.x = (_scaleWidth - bitmap.width)/2;
			bitmap.y = (_scaleHeight - bitmap.height)/2;
			Tweener.removeTweens(_loader);
			_loader.alpha = 0;
			
			Tweener.addTween(view.border, {height:bitmap.height, width:bitmap.width, y:bitmap.y, x:bitmap.x, time:0.3, onComplete:doLoader});
			
			Tweener.addTween(_loadIndicator, {alpha:0, time:0.3, transition:Equations.easeNone, onComplete:view.removeChild, onCompleteParams:[_loadIndicator]});
		}
		
		private function doLoader():void
		{
			Tweener.addTween(_loader, {alpha:1, time:0.3, transition:Equations.easeNone});
		}
		
	}
}