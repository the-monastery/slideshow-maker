/**
 * Written By: Nicholas Hillier, Jan 16, 2009
 * This holds chosen photos in seqence in the Timeline 
 */
package com.cbc.ourgame.slideshowmaker.components.timeline
{
	import assets.timeline.TimelineItemAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.cbc.ourgame.slideshowmaker.model.datastructures.TimelineData;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	
	public class TimelineItem
	{
		private var _bitmap:Bitmap;
		private var _view:TimelineItemAsset;
		private var _node:int;
		private var _timelineData:TimelineData;
		private var _callBack:Function;
		
		private var _isPreview:Boolean;
		private var _isHidden:Boolean;
		private var _isCurrent:Boolean;
		
		public function get view():TimelineItemAsset { return _view; } 
		public function get node():int { return _node; }
		public function get timelineData():TimelineData { return _timelineData; }
		public function get isPreview():Boolean { return _isPreview; }
		public function get isHidden():Boolean { return _isHidden; }
		public function set isCurrent(value:Boolean):void { _isCurrent = value; }
		/**
		 * 
		 * @param view - A TimelineItemAsset, made in the libray
		 * @param node - The sequence number associated with this object in the Timeline
		 * 
		 */
		public function TimelineItem(view:TimelineItemAsset, node:int, callBack:Function)
		{
			_bitmap = new Bitmap();
			_view = view;
			_node = node;
			_callBack = callBack;
			_view.label.text = node.toString();
			_view.holder.addChild(_bitmap);
			
			_isPreview = false;
			_isHidden = false;
			_isCurrent = false;
		}
		
		private function enable():void
		{
			_view.buttonMode = true;
			_view.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_view.addEventListener(MouseEvent.ROLL_OVER, onRollover);
			_view.addEventListener(MouseEvent.ROLL_OUT, onRollout);
		}
		
		private function disable():void
		{
			_view.buttonMode = false;
			_view.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_view.removeEventListener(MouseEvent.ROLL_OVER, onRollover);
			_view.removeEventListener(MouseEvent.ROLL_OUT, onRollout);
		}
		
		public function show(parent:DisplayObjectContainer, level:int):void
		{
			parent.addChildAt(_view, level);	
		}
		
		public function hide(parent:DisplayObjectContainer):void
		{
			parent.removeChild(_view);	
		}
		
		public function addPhoto(timelineData:TimelineData):void
		{
			if(timelineData != null)
			{
				_isPreview = false;
				_bitmap.alpha = 1;
				_timelineData = timelineData;
				_bitmap.bitmapData = null;
				_bitmap.bitmapData = timelineData.bitmapData;
				enable();
			}
		}
		
		public function clearPhoto():void
		{
			_timelineData = null;
			_bitmap.bitmapData = null;
			onRollout(null);
			disable();
		}
		
		public function addSwapPreview(bitmapData:BitmapData):void
		{
			_isPreview = true;
			_bitmap.alpha = 0.5;
			_bitmap.bitmapData = bitmapData;
		}
		
		public function removeSwapPreview():void
		{
			_isPreview = false;
			_bitmap.bitmapData = null;
		}
		
		public function hideContent():void
		{
			_isHidden = true;
			_bitmap.visible = false;
		}
		
		public function showContent():void
		{
			_isHidden = false;
			_bitmap.visible = true;
		}
		
		private function onRollout(e:MouseEvent):void
		{
			if(!_isCurrent)
			Tweener.addTween(_view.actionState, {_color:0xFFFFFF, time:0.1, transition:Equations.easeNone});	
		}
		
		private function onRollover(e:MouseEvent):void
		{
			Tweener.addTween(_view.actionState, {_color:0x0093C6, time:0.1, transition:Equations.easeNone});
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			_callBack(this);
			_timelineData = null;
			_bitmap.bitmapData = null;
			disable();
		}
		
	}
}