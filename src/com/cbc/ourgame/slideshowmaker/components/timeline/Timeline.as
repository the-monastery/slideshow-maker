/**
 * Written By: Nicholas Hillier, Jan 16, 2009
 * The Resevoir or holder of the sequence of chosen photos from the Pluck Library 
 */
package com.cbc.ourgame.slideshowmaker.components.timeline
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.TimelineData;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;

	[Event (name="timelineDrag", type="com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent")]

	public class Timeline extends Sprite
	{
		private var _items:Array;
		private var _currentTarget:TimelineItem;
		private var _dragItem:TimelineItem;
		private var _dragType:String;
		private var _previewData:TimelineData;
		
		public function set dragType(value:String):void { _dragType = value; }
		
		public function get data():Array
		{
			var content:Array = [];
			
			for each(var item:TimelineItem in _items)
				if(item.timelineData) content.push(item.timelineData.photoData);
			
			return content;
		}
		
		/**
		 * 
		 * @param items - expects and array of TimelineItemAssets from the library
		 * 
		 */
		public function Timeline(items:Array)
		{
			_items = [];
			for(var i:int = 0; i < items.length; i++)
			{ 
				var item:TimelineItem = new TimelineItem(items[i], i+1, onMoveTimelineItem);
				_items.push(item);
			}
		}
		
		public function deleteCurrent():void
		{
			_currentTarget.isCurrent = false;	
			if(_currentTarget) _currentTarget.clearPhoto();
		}
		
		public function show(parent:DisplayObjectContainer, level:int):void
		{
			for each(var item:TimelineItem in _items) item.show(parent, level);
		}
		
		public function hide(parent:DisplayObjectContainer):void
		{
			for each(var item:TimelineItem in _items) item.hide(parent);
		}
		
		public function dropTest(data:TimelineData):void
		{
			if(_currentTarget) _currentTarget.addPhoto(data);
			
			for each(var item:TimelineItem in _items)
			{
				if(item.isHidden) item.showContent();
				if(item.isPreview) item.addPhoto(_previewData);
			}
		}
		
		public function dragTest(testObject:Sprite):void
		{
			for each(var item:TimelineItem in _items)	
			{	
				if(testObject.hitTestObject(item.view))
				{
					setHighlight(item);
					if(_dragType == TimelineDataEvent.TIMELINE_DRAG && _currentTarget.timelineData) 
					{
						swapData();
					}	
					//if(item.timelineData == null) clearAllPreviews
				}
				else 
				{
					if(_currentTarget == item) _currentTarget = null;
					removeHighlight(item);
				}
				
				if(item.isHidden && item != _currentTarget) item.showContent();
			}
			
			if(_currentTarget == null || !_currentTarget.timelineData)
				for each(var timelineItem:TimelineItem in _items)
					if(timelineItem.isPreview) timelineItem.removeSwapPreview();
		}
		
		private function swapData():void
		{
			_previewData = _currentTarget.timelineData;
			_dragItem.addSwapPreview(_previewData.bitmapData);
			_currentTarget.hideContent();
		}
			
		private function setHighlight(item:TimelineItem):void
		{
			if(_currentTarget && _currentTarget.isHidden) _currentTarget.showContent();
			
			for each(var timelineItem:TimelineItem in _items)
				timelineItem.isCurrent = false;
			
			_currentTarget = item;
			_currentTarget.isCurrent = true;
			Tweener.addTween(item.view.actionState, {_color:0x0093C6, time:0.1, transition:Equations.easeNone});
		}
		
		private function removeHighlight(item:TimelineItem):void
		{
			Tweener.addTween(item.view.actionState, {_color:0xFFFFFF, time:0.1, transition:Equations.easeNone});
		}
		
		private function onMoveTimelineItem(item:TimelineItem):void
		{
			_dragItem = item;
			dispatchEvent(new TimelineDataEvent(TimelineDataEvent.TIMELINE_DRAG, item.timelineData));
		}
		
	}
}