/**
 * Written By: Nicholas Hillier Jan 20, 2009 
 */
package com.cbc.ourgame.slideshowmaker.components.thumblib
{
	import assets.thumbnail.ThumbnailAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.PhotoData;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.TimelineData;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	
	[Event (name="libraryDrag", type="com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent")]
	
	public class Thumbnail extends ThumbnailAsset
	{
		private var _timelineData:TimelineData;
		private var _isLoaded:Boolean;
		private var _callBack:Function;
		private var _bitmapData:BitmapData;
		private const _shadow:DropShadowFilter = new DropShadowFilter(2,45,0x999999,1,1,1,1,1);
		
		public function get timelineData():TimelineData {	return _timelineData; }
		
		public function Thumbnail(photo:PhotoData, callback:Function)
		{
			_timelineData = new TimelineData(photo);
			_callBack = callback;
			_isLoaded = false;
			mouseChildren = false;
			shadow.filters = [_shadow];
		}
		
		public function enable():void
		{
			buttonMode = true;
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}
		
		public function disable():void
		{
			buttonMode = false;
			removeEventListener(MouseEvent.ROLL_OVER, onRollOver);
			removeEventListener(MouseEvent.ROLL_OUT, onRollOut);
			removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}
		
		public function addView(bitmap:Bitmap, isError:Boolean = false):void
		{
			if(bitmap)
			{
				Tweener.addTween(loadIndicator, {alpha:0, time:0.3, transition:Equations.easeNone, onComplete:killIndicator});
				thumbHolder.alpha = 0;
				thumbHolder.addChild(bitmap);
				Tweener.addTween(thumbHolder, {alpha:1, time:0.3, transition:Equations.easeNone});
				_bitmapData = bitmap.bitmapData;
				_timelineData.bitmapData = bitmap.bitmapData;
				if(!isError) enable();
				else disable();
			}
		}
		
		private function killIndicator():void
		{
			loadIndicator = null;
		}
		
		private function onRollOver(e:Event):void
		{
			shadow.filters = [];
			Tweener.addTween(actionState, {_color:0x0093C6, time:0.3, transition:Equations.easeNone});
		}
		
		private function onRollOut(e:Event):void
		{
			shadow.filters = [_shadow];
			Tweener.addTween(actionState, {_color:0xFFFFFF, time:0.3, transition:Equations.easeNone});
		}
		
		private function onMouseDown(e:MouseEvent):void
		{
			_callBack(this);
			dispatchEvent(new TimelineDataEvent(TimelineDataEvent.LIBRARY_DRAG, _timelineData));
		}
				
	}
}