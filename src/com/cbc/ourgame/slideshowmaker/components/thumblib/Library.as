package com.cbc.ourgame.slideshowmaker.components.thumblib
{
	import assets.library.ImageLibraryAsset;
	
	import com.cbc.ourgame.slideshowmaker.AppFacade;
	import com.cbc.ourgame.slideshowmaker.components.ui.ToolTip;
	import com.cbc.ourgame.slideshowmaker.events.MeasurementEvent;
	import com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.PhotoData;
	import com.ghostmonk.net.BulkImageLoader;
	import com.ghostmonk.shapes.InnerShadowRectangle;
	
	import flash.display.Bitmap;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	[Event (name="mouseWheel", type="flash.events.MouseEvent")]
	[Event (name="addPhoto", type="com.cbc.ourgame.slideshowmaker.events.PhotoEvent")]
	[Event (name="libraryHeight", type="com.cbc.ourgame.slideshowmaker.events.MeasurementEvent")]
	[Event (name="libraryDrag", type="com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent")]
	
	public class Library extends EventDispatcher
	{
		private var _view:ImageLibraryAsset;
		private var _thumbLoader:BulkImageLoader;
		private var _loadTracker:int;
		private var _items:Array;
		private var _loadCue:Array;
		private var _maxY:Number;
		private var _minY:Number;
		private var _mask:InnerShadowRectangle;
		private var _originHeight:Number;
		
		public function get view():ImageLibraryAsset{return _view;}
		public function get items():Array { return _items; }
		
		public function Library(view:ImageLibraryAsset, brokenImgURL:String)
		{
			_view = view;
			_mask = new InnerShadowRectangle(0, _view.width + 7, _view.height);
			_mask.x = _view.x;
			_mask.y = _view.y - 3;
			_view.mask = _mask;
			_view.hitarea.width = _mask.width;
			_maxY = _view.y;
			
			_thumbLoader = new BulkImageLoader(AppFacade.SMALL_BROKEN_IMG, 10);
			
			_originHeight = view.height;
		}
		
		public function createGallery(photos:Array):void
		{
			var row:int;
			var column:int;
			
			_view.y = _maxY;
			_loadTracker = 0;
			
			for each(var thing:Thumbnail in _items)
			{
				view.removeChild(thing);
				thing = null;
			}
			
			_items = [];
			_loadCue = [];
			_view.hitarea.height = 1;
			for(var i:int = 0; i<photos.length; i++)
			{
				var itemData:PhotoData = photos[i];
				var item:Thumbnail = new Thumbnail(itemData, onMoveThumbnail);
				item.addEventListener(TimelineDataEvent.LIBRARY_DRAG, onSendBitmapData);
				_items.push(item);
				_loadCue.push(itemData.small);
				
				column = i % 4;
				item.x = column*(item.width + 2);
				
				row = Math.floor(i/4);
				item.y = row*(item.height + 2);
				
				view.addChild(item);
			}
			_minY = _maxY - view.height + _originHeight - 10;
			//hitarea resized to reflect dimensions of the view, good for reading scroll events
			_view.hitarea.height = _view.height;
			
			//Always check the height of the hit area when loading in a library... disable scroll 
			//wheel if there is no need to scroll
			if(_view.hitarea.height <= 289) _view.removeEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			else _view.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			
			dispatchEvent(new MeasurementEvent(MeasurementEvent.HEIGHT_MEASUREMENT, view.height));
			loadPhotos();
		}
		
		public function scroll(percent:Number):void
		{
			view.y = int(-(_maxY - _minY) * percent + _maxY);
		}
		
		private function loadPhotos():void
		{
			_thumbLoader.loadList(_loadCue, thumbLoaded, brokenThumb); 
		}
		
		private function thumbLoaded(bitmap:Bitmap, index:int):void
		{
			Thumbnail(_items[index]).addView(bitmap);
		}
		
		private function brokenThumb(bitmap:Bitmap, index:int):void
		{
			Thumbnail(_items[index]).addView(bitmap, true);
		}
		
		private function onMoveThumbnail(item:Thumbnail):void
		{
			dispatchEvent(new TimelineDataEvent(TimelineDataEvent.LIBRARY_DRAG, item.timelineData));
		}
		
		private function onSendBitmapData(e:TimelineDataEvent):void
		{
			dispatchEvent(e);
		}
		
		private function onMouseWheel(e:MouseEvent):void
		{
			dispatchEvent(e);
		}
		
	}
}