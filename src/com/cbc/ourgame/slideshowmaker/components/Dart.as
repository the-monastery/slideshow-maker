package com.cbc.ourgame.slideshowmaker.components
{
	import com.doubleclick.dartshell.ad.instream.events.DartInStreamAdLoadedEvent;
	import com.doubleclick.dartshell.common.events.LogEvent;
	import com.doubleclick.dartshell.component.adholder.AdHolder;
	import com.doubleclick.dartshell.errors.events.DartShellErrorEvent;
	import com.doubleclick.dartshell.events.DartShellLoadedEvent;
	
	import flash.display.Sprite;
	import flash.errors.IOError;
	import flash.events.Event;
	
	public class Dart extends AdHolder
	{ 
		private var _clickDisplay:Sprite;
		private var _isShellLoaded:Boolean;
		
		public function isShellLoaded():Boolean { return _isShellLoaded; }
		
		public function Dart(tagURL:String, clickDisplay:Sprite)
		{
			adTagURL = tagURL;
			_clickDisplay = clickDisplay;
			
			addEventListener(LogEvent.TYPE, onLogEvent); 
			addEventListener(DartShellLoadedEvent.TYPE, onShellLoaded); 
			addEventListener(DartShellErrorEvent.TYPE, onShellError);  
			addEventListener(DartInStreamAdLoadedEvent.TYPE, onAdLoaded);  
			
			addEventListener(Event.ADDED_TO_STAGE, enable);
			addEventListener(Event.REMOVED_FROM_STAGE, disable);
			
			_isShellLoaded = false;
		}
		
		private function enable(e:Event = null):void
		{
			setClickTrackingMC(_clickDisplay);
		}
		
		private function disable(e:Event = null):void
		{
			setClickTrackingMC(null);
		}
		
		private function onLogEvent(e:LogEvent):void
		{
			trace(e.message);
		}
		
		private function onShellLoaded(e:DartShellLoadedEvent):void
		{
			_isShellLoaded = true;
			loadNewAd();
		}
		
		private function onShellError(e:DartShellErrorEvent):void
		{
			trace(e.getError().getCause());
			trace(e.getError().getStackTrace());
		}
		
		private function onAdLoaded(e:DartInStreamAdLoadedEvent):void
		{
			trace(e.getDartInStreamAd().getAuthor());
			trace(e.getDartInStreamAd().getClickThroughURL());
			trace(e.getDartInStreamAd().getTitle());
			trace(e.getDartInStreamAd().getDartId());
		}

	}
}