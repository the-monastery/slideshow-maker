package com.cbc.ourgame.slideshowmaker.components.player
{
	import assets.slideshowplayer.PreviewScreenAsset;
	import assets.slideshowplayer.SlideshowPlayerBtn;
	import assets.volume.VolumeControlAsset;
	
	import com.cbc.ourgame.slideshowmaker.components.audio.VolumeController;
	import com.cbc.ourgame.slideshowmaker.components.ui.FrameButton;
	import com.cbc.ourgame.slideshowmaker.events.SlideshowEvent;
	import com.cbc.ourgame.slideshowmaker.events.SubmitGalleryEvent;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	[Event (name="slideshowStart", type="com.cbc.ourgame.slideshowmaker.events.SlideshowEvent")]
	[Event (name="slideshowStop", type="com.cbc.ourgame.slideshowmaker.events.SlideshowEvent")]
	[Event (name="submitGalleryName", type="com.cbc.ourgame.slideshowmaker.events.SubmitGalleryEvent")]
	
	public class SlideshowPlayer extends Sprite
	{	
		private var _slideshow:Slideshow;
		private var _nameGalleryForm:NameGalleryForm;
		private var _play:FrameButton;
		private var _back:FrameButton;
		private var _publishButton:FrameButton;
		private var _volumeControl:VolumeController;
		private var _parent:DisplayObjectContainer;
		private var _isStandAlone:Boolean;
		private var _standAloneTitleField:TextField;
		
		private var _isPlaying:Boolean;
		
		public function get standAloneTitleField():TextField { return _standAloneTitleField; }
		public function get volume():Number { return _volumeControl.volume; }
		public function set volume(value:Number):void { _volumeControl.volume = value; }
		
		public function SlideshowPlayer(screen:PreviewScreenAsset, play:SlideshowPlayerBtn, back:SlideshowPlayerBtn, share:SlideshowPlayerBtn, volume:VolumeControlAsset)
		{
			_isStandAlone = false;
			_parent = play.parent;
			
			_slideshow = new Slideshow(screen, onSlideShowLoaded, onPlayToggle);
			_slideshow.addEventListener(SlideshowEvent.SLIDESHOW_END, onSlideShowComplete);
			_slideshow.addEventListener(SlideshowEvent.AD_LOADED, onAdLoaded);
			_slideshow.addEventListener(SlideshowEvent.AD_COMPLETE, onAdComplete);
			
			_nameGalleryForm = new NameGalleryForm();
			_nameGalleryForm.addEventListener(SubmitGalleryEvent.SUBMIT_GALLERY_NAME, onSubmitGalleryName);
			_nameGalleryForm.x = (_slideshow.view.width - _nameGalleryForm.width)/2;
			_nameGalleryForm.y = (_slideshow.view.height - _nameGalleryForm.height)/2;
			
			play.label.gotoAndStop("play");
			back.label.gotoAndStop("back");
			share.label.gotoAndStop("publish");
			
			_play = new FrameButton(play);
			_back = new FrameButton(back);
			_publishButton = new FrameButton(share);
			_volumeControl = new VolumeController(volume);
			
			_play.addCallback(onPlayToggle);
			_back.addCallback(onBack);
			_publishButton.addCallback(onPublish);
			
			_isPlaying = false;
		}
		
		public function isStandAlone(titleField:TextField):void
		{
			_standAloneTitleField = titleField;
			_isStandAlone = true;
			_play.disable(true);
			_back.disable(true);
		}
		
		public function setReturnedKey(key:String):void
		{
			_nameGalleryForm.displayLink(key);
		}
		
		public function show():void
		{
			_play.view.label.gotoAndStop("play");
			_isPlaying = false;
			dispatchEvent(new SlideshowEvent(SlideshowEvent.SLIDESHOW_END));
			_parent.addChild(_play.view);
			_parent.addChild(_back.view);
			_parent.addChild(_publishButton.view);	
			_parent.addChild(_volumeControl.view);
			_slideshow.show(_parent); 
		}
		
		public function hide():void
		{
			_slideshow.hide(_parent);
			_parent.removeChild(_play.view);
			_parent.removeChild(_back.view);
			_parent.removeChild(_publishButton.view);
			_parent.removeChild(_volumeControl.view);
			dispatchEvent(new SlideshowEvent(SlideshowEvent.SLIDESHOW_END));
		}
		
		public function loadSlideShow(photos:Array):void
		{
			_play.disable(true);
			_back.disable(true);
			_slideshow.loadSlideShow(photos);
		}
		
		private function onPlayToggle(e:MouseEvent):void
		{
			_isPlaying = !_isPlaying;
			
			if(_isPlaying)
			{
				_play.view.label.gotoAndStop("stop");
				_slideshow.play();
				dispatchEvent(new SlideshowEvent(SlideshowEvent.SLIDESHOW_START));
			}
			else
			{
				dispatchEvent(new SlideshowEvent(SlideshowEvent.SLIDESHOW_PAUSE));
				_slideshow.pause();
				_play.view.label.gotoAndStop("play");
			}
		}
		
		private function onBack(e:MouseEvent):void
		{
			dispatchEvent(new SlideshowEvent(SlideshowEvent.SLIDESHOW_RESTART));
			_slideshow.stop();
			_play.view.label.gotoAndStop("play");
			_isPlaying = false;
		}
		
		private function onPublish(e:MouseEvent):void
		{
			_slideshow.pause();
			dispatchEvent(new SlideshowEvent(SlideshowEvent.SLIDESHOW_PAUSE));
			_nameGalleryForm.buildIn(_slideshow.view.stage);
			_play.view.label.gotoAndStop("play");
		}
		
		private function onSlideShowLoaded():void
		{
			_play.enable();
			_back.enable();
		}
		
		private function onSubmitGalleryName(e:SubmitGalleryEvent):void
		{
			dispatchEvent(e);
			_publishButton.disable(true);
			_isPlaying = false;
		}
		
		private function onSlideShowComplete(e:SlideshowEvent):void
		{
			dispatchEvent(e);
			_isPlaying = false;
			_play.view.label.gotoAndStop("play");
		}
		
		private function onAdLoaded(e:SlideshowEvent):void
		{
			_play.disable(true);
			_publishButton.disable(true);
			_back.disable(true);
			dispatchEvent(e);
		}
		
		private function onAdComplete(e:SlideshowEvent):void
		{
			_play.enable();
			_publishButton.enable();
			_back.enable();
			dispatchEvent(e);
		}
		
	}
}