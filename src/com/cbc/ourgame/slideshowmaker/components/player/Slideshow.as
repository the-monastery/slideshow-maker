package com.cbc.ourgame.slideshowmaker.components.player
{
	import assets.ScreenTileAsset;
	import assets.slideshowplayer.PreviewScreenAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.cbc.ourgame.slideshowmaker.AppFacade;
	import com.cbc.ourgame.slideshowmaker.components.DartAd;
	import com.cbc.ourgame.slideshowmaker.components.ui.Clock;
	import com.cbc.ourgame.slideshowmaker.components.ui.FrameButton;
	import com.cbc.ourgame.slideshowmaker.events.SlideshowEvent;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.PhotoData;
	import com.ghostmonk.net.BulkImageLoader;
	import com.ghostmonk.shapes.BitmapFillRect;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.EventDispatcher;
	
	[Event (name="slideshowStart", type="com.cbc.ourgame.slideshowmaker.events.SlideshowEvent")]
	[Event (name="slideshowPause", type="com.cbc.ourgame.slideshowmaker.events.SlideshowEvent")]
	[Event (name="slideshowResume", type="com.cbc.ourgame.slideshowmaker.events.SlideshowEvent")]
	[Event (name="slideshowEnd", type="com.cbc.ourgame.slideshowmaker.events.SlideshowEvent")]
	[Event (name="adLoaded", type="com.cbc.ourgame.slideshowmaker.events.SlideshowEvent")]
	[Event (name="adComplete", type="com.cbc.ourgame.slideshowmaker.events.SlideshowEvent")]
	
	public class Slideshow extends EventDispatcher
	{
		private var _screen:BitmapFillRect;
		private var _display:PreviewScreenAsset;
		private var _loader:BulkImageLoader;
		private var _currentSlide:Slide;
		private var _slideShowItems:Array;
		private var _currentSlideIndex:int;
		private var _callBack:Function;
		private var _isPaused:Boolean;
		private var _mask:Bitmap;
		private var _ad:DartAd;
		
		private var _loadTracker:int;
		private var _totalSlides:int;
		private var _isEnded:Boolean;
		private var _majorPlay:FrameButton;
		private var _clock:Clock;
		
		public function get view():Sprite { return _display; }
		
		public function Slideshow(display:PreviewScreenAsset, callBack:Function, innerPlayClick:Function)
		{
			_display = display;
			createMask();
			_display.indicator.alpha = 0;
			_callBack = callBack;
			_slideShowItems = [];
			
			_loader = new BulkImageLoader(AppFacade.LARGE_BROKEN_IMG, 1);
			_loader.completeCall(onSlideShowLoaded);
			
			_isPaused = false;
			_isEnded = true;
			
			_screen = new BitmapFillRect(new ScreenTileAsset(0,0));
			_screen.make(_display.width, _display.height);
			_display.removeChild(_display.majorPlayBtn);
			
			_majorPlay = new FrameButton(_display.majorPlayBtn);
			_majorPlay.addCallback(innerPlayClick);
			_clock = new Clock(20,0xFFFFFF,0x1A4364);
			_clock.x = 25;
			_clock.y = 25;
		}
		
		public function show(parent:DisplayObjectContainer):void
		{
			_currentSlideIndex = 0;
			parent.addChild(_display);
		}
		
		public function hide(parent:DisplayObjectContainer):void
		{
			for each(var item:Slide in _slideShowItems)
				item.destruct();
			
			parent.removeChild(_display);	
			_loader.destruct();
			_isPaused = false;	
			_clock.stop();
			removeScreen();	
		}
		
		public function loadSlideShow(photos:Array):void
		{
			Tweener.addTween(_display.indicator, {alpha:1, time:0.3, transition:Equations.easeNone});
			var photoURLs:Array = [];
			for each(var photoData:PhotoData in photos)
				photoURLs.push(photoData.large); 	
			
			_loadTracker = 1;
			_totalSlides = photoURLs.length;
			updateLoadMsg();
			_display.msg.visible = true;
			_loader.loadList(photoURLs, onLoaded, onSlideLoadError);
		}
		
		public function play():void
		{
			if(_isPaused) 
			{
				_clock.resume();
				_isPaused = false;
			}
			else
			{
				_clock.start(_slideShowItems.length*7);
				showNewSlide();	
			}
			
			for each(var item:Slide in _slideShowItems)
				item.resume();
				
			removeScreen();
		}
		
		public function pause():void
		{
			if(!_isEnded)
			{
				_isPaused = true;
				for each(var item:Slide in _slideShowItems)
					item.pause();
					_clock.pause();
			}	
			else
			{	
				_clock.stop();
			}
			addScreen();
		}
		
		public function stop():void
		{
			_currentSlideIndex = 0;
			_isEnded = true;
			_isPaused = false;
			for each(var item:Slide in _slideShowItems)
				item.kill();
			
			view.addChild(_slideShowItems[0]);
			_clock.stop();
			addScreen();
		}
		
		private function onAdLoaded(bitmap:Bitmap, url:String):void
		{
			var ad:Slide = new Slide(bitmap, 
									adComplete, 
									577, 
									410, 
									TweenType.CENTER_OUT);
			_currentSlide = ad;
			ad.click(url);
			view.addChild(ad);
			ad.buildIn();
			_clock.stop();
			dispatchEvent(new SlideshowEvent(SlideshowEvent.AD_LOADED));
		}
		
		private function adComplete(slide:Slide):void
		{
			slide.remove();
			dispatchEvent(new SlideshowEvent(SlideshowEvent.SLIDESHOW_END));
			stop();
			dispatchEvent(new SlideshowEvent(SlideshowEvent.AD_COMPLETE));
		}
		
		private function onLoaded(bitmap:Bitmap, index:int):void
		{
			_loadTracker++;
			updateLoadMsg();
			var tween:int = bitmap.height > bitmap.width ? Math.floor(Math.random()*2)+18 : Math.floor(Math.random()*24);
			_slideShowItems[index] = new Slide(bitmap, slideComplete, 577/* _screen.width */, 410/* _screen.height */, tween);
		}
		
		private function onSlideShowLoaded():void
		{
			Tweener.addTween(_display.indicator, {alpha:0, time:0.3, transition:Equations.easeNone});
			_display.msg.visible = false;
			_callBack();
			view.addChild(_slideShowItems[0]);
			
			addScreen();
		}
		
		private function showNewSlide():void
		{
			_isEnded = false;
			_currentSlide = _slideShowItems[_currentSlideIndex];
			_display.addChild(_currentSlide);
			_display.addChild(_clock);
			_currentSlide.buildIn();
		}
		
		private function slideComplete(slide:Slide):void
		{
			slide.remove();
			_currentSlideIndex++;
			if(_currentSlideIndex < _slideShowItems.length) 
			{
				showNewSlide();
			}
			else 
			{
				_ad = new DartAd(onAdLoaded, onAdLoadError);
				_isEnded = true;
			}
		}
		
		private function onAdLoadError():void
		{
			dispatchEvent(new SlideshowEvent(SlideshowEvent.SLIDESHOW_END));
			stop();
		}
		
		private function createMask():void
		{
			_mask = new Bitmap(new BitmapData(_display.width, _display.height, true, 0));
			_mask.bitmapData.draw(_display);
			_mask.x = _display.x;
			_mask.y = _display.y;
			_display.mask = _mask;
		}
		
		private function addScreen():void
		{
			_display.addChild(_screen);
			_display.addChild(_display.majorPlayBtn);
		}
		
		private function removeScreen():void
		{
			try
			{
				_display.removeChild(_screen);
				_display.removeChild(_display.majorPlayBtn);
			}
			catch(e:ArgumentError){}
		}
		
		private function updateLoadMsg():void
		{
			_display.msg.htmlText = "<b>Loading "+_loadTracker+" of "+_totalSlides+"...</b>";
		}
		
		private function onSlideLoadError(bitmap:Bitmap):void
		{
			
		}

	}
}