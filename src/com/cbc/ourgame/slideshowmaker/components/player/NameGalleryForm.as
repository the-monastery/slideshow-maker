package com.cbc.ourgame.slideshowmaker.components.player
{
	import assets.ScreenTileAsset;
	import assets.form.NameGalleryFormAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.cbc.ourgame.slideshowmaker.components.ui.FrameButton;
	import com.cbc.ourgame.slideshowmaker.events.SubmitGalleryEvent;
	import com.ghostmonk.shapes.BitmapFillRect;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFieldAutoSize;

	[Event (name="submitGalleryName", type="com.cbc.ourgame.slideshowmaker.events.SubmitGalleryEvent")]

	public class NameGalleryForm extends NameGalleryFormAsset
	{
		private var _submitButton:FrameButton;
		private var _cancelButton:FrameButton;
		private var _tweenerBitmap:Bitmap;
		private var _screen:BitmapFillRect;
		
		public function NameGalleryForm()
		{
			removeChild(indicator);
			_tweenerBitmap = new Bitmap(new BitmapData(width, height, true, 0));
			_screen = new BitmapFillRect(new ScreenTileAsset(0,0));
			
			submitBtn.label.gotoAndStop("publish");
			cancel.label.gotoAndStop("cancel");
			_submitButton = new FrameButton(submitBtn);
			_cancelButton = new FrameButton(cancel);
			
			_cancelButton.addCallback(buildOut);
			_submitButton.addCallback(onSubmit);
			_submitButton.disable(true, true);
			
			input.addEventListener(Event.CHANGE, onTextChange);
		}
		
		public function disable():void
		{
			_submitButton.disable();
		}
		
		public function enable():void
		{
			_submitButton.enable();
		}
		
		public function buildIn(parent:DisplayObjectContainer):void
		{
			
			_screen.make(parent.stage.stageWidth, parent.stage.stageHeight);
			
			clearDisplay();
			parent.addChild(this);
			
			alpha = 0;
			
			showDisplay();
			_screen.alpha = 0;
			
			stage.addChildAt(_screen, stage.getChildIndex(this));
			
			Tweener.addTween(_screen, {alpha:1, time:0.3, transition:Equations.easeNone, onComplete:showDisplay});
			Tweener.addTween(this, {alpha:1, time:0.3, transition:Equations.easeNone, onComplete:showDisplay});
		}
		
		public function buildOut(e:Event = null):void
		{
			clearDisplay();
			
			Tweener.addTween(_screen, {alpha:0, time:0.3, transition:Equations.easeNone, onComplete:stage.removeChild, onCompleteParams:[_screen]});
			Tweener.addTween(this, {alpha:0, time:0.3, transition:Equations.easeNone, onComplete:parent.removeChild, onCompleteParams:[this]});
		}
		
		public function displayLink(key:String):void
		{
			message.autoSize = TextFieldAutoSize.LEFT;
			removeChild(indicator);
			//var message:String = "Your gallery has been successfully published to the following link.";
			var msg:String = "The publish functionality has been disabled for this demo.";
			Tweener.addTween(message, {_text:msg, time:0.3, transition:Equations.easeNone});
			input.visible = true;
			inputBG.visible = true;
			input.text = "http://projects.ghostmonk.com/as3/slideshow-maker/";
			removeChild(_submitButton.view);
			_cancelButton.enable();
			cancel.label.gotoAndStop("return");
			input.multiline = true;
			input.wordWrap = true;
			input.autoSize = TextFieldAutoSize.LEFT;
			input.selectable = true;
			input.stage.focus = input;
			input.setSelection(0, input.text.length);
			
			inputBG.height = input.height + 10;
			cancel.y = inputBG.y + inputBG.height + 10;
			cancel.x = submitBtn.x;
			background.height = cancel.y + cancel.height + 10 - background.y;
		}
		
		private function onSubmit(e:MouseEvent):void
		{
			dispatchEvent(new SubmitGalleryEvent(	SubmitGalleryEvent.SUBMIT_GALLERY_NAME, 
													input.text));
			
			Tweener.addTween(message, {_text:"Your Gallery is being published.", time:0.3, transition:Equations.easeNone});
			//message.text = "Your Gallery is being published.";
			input.visible = false;
			inputBG.visible = false;
			addChild(indicator);
			indicator.alpha = 0;
			Tweener.addTween(indicator, {alpha:1, time:0.3, transition:Equations.easeNone});
			_submitButton.disable(true);
			_cancelButton.disable(true);
		}
		
		private function onCancel(e:MouseEvent):void
		{
			clearDisplay();
		}
		
		private function onTextChange(e:Event):void
		{
			if(input.text.length > 0)
			{
				_submitButton.enable();
			}
			else
			{
				_submitButton.disable(true);
			}
		}
		
		private function showDisplay():void
		{
			addChild(submitBtn);
			addChild(message);
			addChild(input);
			input.stage.focus = input;
			try { removeChild(_tweenerBitmap) } catch(e:Error){};
		}
		
		private function clearDisplay():void
		{
			_tweenerBitmap.bitmapData.dispose();
			_tweenerBitmap.bitmapData = new BitmapData(width, height, true, 0);
			_tweenerBitmap.bitmapData.draw(this);
			addChild(_tweenerBitmap);
			
			try
			{
				removeChild(submitBtn);
				removeChild(message);
				removeChild(input);
			}
			catch(e:Error)
			{
				
			}
		}
		
	}
}