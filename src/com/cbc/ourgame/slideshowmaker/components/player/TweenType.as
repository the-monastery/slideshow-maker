package com.cbc.ourgame.slideshowmaker.components.player
{
	public class TweenType
	{
		
		//public static const LEFT_UP_OUT:TweenDirection = new TweenDirection(HorizontalDirection.LEFT, VerticalDirection.UP, ZoomDirection.OUT);
		
		public static const LEFT_UP_OUT:int = 0;
		public static const LEFT_UP_IN:int = 1;
		public static const LEFT_DOWN_OUT:int = 2;
		public static const LEFT_DOWN_IN:int = 3;
		public static const LEFT_DOWN:int = 4;
		public static const LEFT_UP:int = 5;
		public static const LEFT_IN:int = 6;
		public static const LEFT_OUT:int = 7;
		
		public static const RIGHT_UP_OUT:int = 8;
		public static const RIGHT_UP_IN:int = 9;
		public static const RIGHT_DOWN_OUT:int = 10;
		public static const RIGHT_DOWN_IN:int = 11;
		public static const RIGHT_DOWN:int = 12;
		public static const RIGHT_UP:int = 13;
		public static const RIGHT_IN:int = 14;
		public static const RIGHT_OUT:int = 15;
		
		public static const UP_IN:int = 16;
		public static const UP_OUT:int = 17;
		public static const UP:int = 18;
		public static const DOWN:int = 19;
		public static const CENTER_IN:int = 20;
		public static const CENTER_OUT:int = 21;
		
		public static const RIGHT:int = 22;
		public static const LEFT:int = 23;
		
		private var _type:Array;
		
		public function get type():Array { return _type; }
		
		public function TweenType(type:int)
		{
			
			switch(type)
			{
				case 0: 	_type = [2,0,6]; 	break; //LEFT_UP_OUT >>>>>>>>>>>>
				case 1: 	_type = [2,0,5]; 	break; //LEFT_UP_IN >>>>>>>>>>>>>
				case 2: 	_type = [2,1,6]; 	break; //LEFT_DOWN_OUT >>>>>>>>>
				case 3: 	_type = [2,1,5]; 	break; //LEFT_DOWN_IN >>>>>>
				case 4: 	_type = [2,1,7]; 	break; //LEFT_DOWN >>>>>>
				case 5: 	_type = [2,0,7]; 	break; //LEFT_UP >>>>>>>>>>>
				case 6: 	_type = [2,4,5]; 	break; //LEFT_IN >>>>>>>>>
				case 7: 	_type = [2,4,6]; 	break; //LEFT_OUT >>>>>>>>>>
				case 8: 	_type = [3,0,6]; 	break; //RIGHT_UP_OUT >>>>>>>>>>
				case 9: 	_type = [3,0,5]; 	break; //RIGHT_UP_IN >>>>>>>>>>>
				case 10: 	_type = [3,1,6]; 	break; //RIGHT_DOWN_OUT >>>>>>>>
				case 11: 	_type = [3,1,5]; 	break; //RIGHT_DOWN_IN >>>>>>>>>
				case 12: 	_type = [3,1,7]; 	break; //RIGHT_DOWN >>>>>>>>>>
				case 13: 	_type = [3,0,7]; 	break; //RIGHT_UP >>>>>>>>>>>>>>
				case 14: 	_type = [3,4,5]; 	break; //RIGHT_IN >>>>>>>>>>>>>>
				case 15: 	_type = [3,4,6]; 	break; //RIGHT_OUT >>>>>>>>>>>>>
				case 16: 	_type = [4,0,5]; 	break; //UP_IN >>>>>>>>>>>>
				case 17: 	_type = [4,0,6]; 	break; //UP_OUT >>>>>>>>>>>>
				case 18: 	_type = [4,0,7]; 	break; //UP >>>>>>>>>>>
				case 19: 	_type = [4,1,7]; 	break; //DOWN >>>>>>>>>
				case 20: 	_type = [4,4,5]; 	break; //CENTER_IN >>>>>>>>>
				case 21: 	_type = [4,4,6]; 	break; //CENTER_OUT >>>>>>>
				case 22:	_type = [3,4,7]; 	break; //RIGHT ************
				case 23:	_type = [2,4,7];	break; //LEFT *************
			}
		}

	}
}