package com.cbc.ourgame.slideshowmaker.components.player
{
	import caurina.transitions.Equations;
	
	public class SlideTween extends Object
	{
		private const UP:int = 0;
		private const DOWN:int = 1;
		private const LEFT:int = 2;
		private const RIGHT:int = 3;
		private const CENTER:int = 4;
		private const ZOOM_IN:int = 5;
		private const ZOOM_OUT:int = 6;
		private const NO_ZOOM:int = 7;
		
		private const SCALE_FACTOR:Number = 1.2;
		
		private var _startX:Number;
		private var _endX:Number;
		
		private var _startY:Number;
		private var _endY:Number; 
		
		private var _startScale:Number;
		private var _endScale:Number;	
		
		private var _time:Number; 
		private var _transition:Function;
		private var _completeCall:Function;
		private var _target:Slide;
		
		private var _maxScale:Number;
		private var _minScale:Number;
		
		private var _yType:int;
		private var _xType:int;
		private var _zoomType:int;
		
		public function set scale(value:Number):void { _endScale = value;}
		
		public function SlideTween(width:Number, height:Number, completeCall:Function, target:Slide, tweenType:TweenType)
		{
			_time = 7;
			_transition = Equations.easeNone;
			_completeCall = completeCall;
			_target = target;
			
			_minScale = Math.max(width/_target.width, height/_target.height);
			_maxScale = _minScale * SCALE_FACTOR;
			
			_xType = tweenType.type[0];
			_yType = tweenType.type[1];
			_zoomType = tweenType.type[2];
			
			setScale();
			startPoint(width, height);
			endPoint(width, height);	
		}
		
		private function setScale():void
		{
			_startScale = _zoomType == ZOOM_OUT ? _maxScale : _minScale;
			_endScale = _zoomType == ZOOM_IN ? _maxScale : _minScale;
		}
		
		public function beginPosition():void
		{
			_target.scaleX = _target.scaleY = _startScale;
			_target.x = _startX;
			_target.y = _startY;
		}
		
		private function startPoint(width:Number, height:Number):void
		{
			_target.scaleX = _target.scaleY = _startScale;
			
			switch(_yType)
			{
				case DOWN: 		_startY = (-_target.height + height); 	break;
				case UP: 		_startY = 0; 							break;
				case CENTER: 	_startY = (height - _target.height)/2; 	break;
			}
			
			switch(_xType)
			{
				case LEFT: 		_startX = 0; 							break;
				case RIGHT: 	_startX = (-_target.width + width);		break;
				case CENTER:  	_startX = (width - _target.width)/2; 	break;
			}
		}
		
		private function endPoint(width:Number, height:Number):void
		{
			_target.scaleX = _target.scaleY = _endScale;
			
			switch(_yType)
			{
				case DOWN: 		_endY = 0; 								break;
				case UP: 		_endY = (-_target.height + height);		break;
				case CENTER: 	_endY = (height - _target.height)/2; 	break;
			}
			
			switch(_xType)
			{
				case LEFT: 		_endX = (-_target.width + width); 		break;
				case RIGHT: 	_endX = 0;								break;
				case CENTER:  	_endX = (width - _target.width)/2; 		break;
			}
		}
		
		public function get tween():Object 
		{
			return {x:_endX, y:_endY, scaleX:_endScale, scaleY:_endScale, time:_time, transition:_transition, onComplete:_completeCall, onCompleteParams:[_target]}
		}
		
	}
}