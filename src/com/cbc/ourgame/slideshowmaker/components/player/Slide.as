package com.cbc.ourgame.slideshowmaker.components.player
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	public class Slide extends Sprite
	{
		private var _view:Bitmap;
		private var _isOnStage:Boolean;
		private var _url:String;
		
		private var _baseTween:SlideTween;
		
		public function Slide(bitmap:Bitmap, completeCall:Function, containerWidth:Number, containerHeight:Number, tweenType:int)
		{
			_view = bitmap;
			_view.smoothing = true;
			addChild(_view);
			alpha = 0;
			
			_baseTween = new SlideTween(containerWidth, containerHeight, completeCall, this, new TweenType(tweenType));
			
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		
		public function click(url:String):void
		{
			_url = url;
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		public function onClick(e:MouseEvent):void
		{
			navigateToURL(new URLRequest(_url), "_blank");
		}
		
		public function kill():void
		{
			Tweener.removeTweens(this);
			if(_isOnStage) parent.removeChild(this);
		}
		
		public function remove():void
		{
			if(_isOnStage)
			{
				Tweener.removeTweens(this);
				Tweener.addTween(this, {alpha:0, time:0.5, transition:Equations.easeNone, onComplete:parent.removeChild, onCompleteParams:[this]});
			} 
		}
		
		public function buildIn():void
		{
			Tweener.addTween(this, _baseTween.tween);
		}
		
		public function pause():void
		{
			Tweener.pauseTweens(this);
		}
		
		public function resume():void
		{
			Tweener.resumeTweens(this);
		}
		
		public function destruct():void
		{
			try
			{
				if(_isOnStage) parent.removeChild(this);
				Tweener.removeTweens(this);
	//TypeError: Error #2007: Parameter child must be non-null.
				//removeChild(_view);
				_view.bitmapData = null;
				_view = null;
				delete this;
				null
			}
			catch(e:Error)
			{
				
			}
		}
		
		private function onAddedToStage(e:Event):void
		{
			_isOnStage = true;
			_baseTween.beginPosition();
			alpha = 0;
			Tweener.addTween(this, {alpha:1, time:0.5, transition:Equations.easeNone});
		}
		
		private function onRemovedFromStage(e:Event):void
		{
			_isOnStage = false
		}

	}
}