package com.cbc.ourgame.slideshowmaker.components.audio
{
	import assets.audiochooser.MenuItemAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;

	public class MenuItem extends MenuItemAsset
	{
		private var _request:URLRequest;
		private var _bg:Bitmap;
		private var _baseColor:uint;
		private var _callback:Function;
		private var _id:String;
		
		public function get request():URLRequest { return _request; }
		public function get id():String { return _id; }
		
		public function MenuItem(content:XML, callback:Function)
		{
			_request = new URLRequest(content.@src);
			label.htmlText = content.@name;
			_id = content.@id;
			_callback = callback;
			_baseColor = label.textColor;
			
			_bg = new Bitmap(new BitmapData(width, height, false, _baseColor));
			_bg.alpha = 0;
			
			mouseChildren = false;
			enable();
		}
		
		public function enable():void
		{
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onClick);
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
		}
		
		public function disable():void
		{
			buttonMode = false;
			removeEventListener(MouseEvent.CLICK, onClick);
			removeEventListener(MouseEvent.ROLL_OVER, onRollOver);
			removeEventListener(MouseEvent.ROLL_OUT, onRollOut);
			onRollOut(null);
		}
		
		private function onRollOver(e:MouseEvent):void
		{
			addChildAt(_bg, 0);
			Tweener.addTween(_bg, {alpha:1, time:0.2, transition:Equations.easeNone});
			label.textColor = 0xFFFFFF;
		}
		
		private function onRollOut(e:MouseEvent):void
		{
			Tweener.addTween(_bg, {alpha:0, time:0.2, transition:Equations.easeNone/* , onComplete:removeChild, onCompleteParams:[_bg] */});
			label.textColor = _baseColor;
		}
		
		private function onClick(e:MouseEvent):void
		{
			_callback(this);
		}
	}
}