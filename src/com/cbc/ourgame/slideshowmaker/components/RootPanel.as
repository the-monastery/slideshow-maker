/**
 * Written By: Nicholas Hillier Jan 16, 2009
 * 
 * Extends a library asset that provides positioning and access to assets used in the most of the project,
 * timeline, audiopanel, ImagePreview, backbutton, scroller and the thumb library are created inside 
 * of this class because the components are already located in this class...  
 */
 package com.cbc.ourgame.slideshowmaker.components
{
	import assets.RootPanelAsset;
	
	import com.cbc.ourgame.slideshowmaker.components.timeline.Timeline;
	import com.cbc.ourgame.slideshowmaker.components.ui.FrameButton;
	import com.cbc.ourgame.slideshowmaker.components.ui.Scroller;
	
	public class RootPanel extends RootPanelAsset
	{
		private var _timeline:Timeline;
		private var _mainToggle:FrameButton;
		private var _scroller:Scroller;
		
		public function get scroller():Scroller { 	return _scroller;	}
		public function get timeline():Timeline { 	return _timeline;	}
		
		public function RootPanel()
		{
			_timeline = new Timeline([timelineItem01, timelineItem02, timelineItem03, timelineItem04, timelineItem05, timelineItem06, timelineItem07, timelineItem08]);
			_scroller = new Scroller(scrollBar);
			show();
		}
		
		public function show():void
		{
			backgroundLineFill.visible = false;
			addChildAt(header, 1);
			addChildAt(editBorder, 1);
			addChild(_scroller.view);
			_timeline.show(this, 1);	
		}
		
		public function hide():void
		{
			backgroundLineFill.visible = true;
			removeChild(header);
			removeChild(editBorder);
			_timeline.hide(this);
			removeChild(_scroller.view);
		}
		
		public function setTitle(value:String):void
		{
			header.mainLabel.htmlText = "<b>"+value+"</b>";
		}
	}
}