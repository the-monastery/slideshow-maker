package com.cbc.ourgame.slideshowmaker.components
{
	import assets.droptesticon.DragDropIconAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.TimelineData;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;

	public class DragDropIcon extends DragDropIconAsset
	{
		private var _dragIcon:Bitmap;
		private var _isActive:Boolean;
		private var _hasData:Boolean;
		private var _timelineData:TimelineData;
		private var _dragType:String;
		
		public function set hasData(value:Boolean):void { 	_hasData = value; 	}
		public function set dragType(value:String):void { 	_dragType = value;	}
		
		public function get hasData():Boolean 			{ 	return _hasData; 	}
		public function get dragType():String 			{ 	return _dragType; 	}
		public function get isActive():Boolean 			{ 	return _isActive; 	}
		
		public function DragDropIcon()
		{
			_dragIcon = new Bitmap();
			_isActive = false;
			_hasData = false;
			holder.addChild(_dragIcon);
		}
		
		public function get timelineData():TimelineData
		{
			return _timelineData;
		}
		
		public function set data(data:TimelineData):void
		{
			_timelineData = data;
			_dragIcon.bitmapData = null;
			_dragIcon.bitmapData = data.bitmapData;
			_hasData = true;
		}
		
		public function show(parent:DisplayObjectContainer):void
		{
			_isActive = true;
			parent.addChild(this);
			Tweener.addTween(this, {alpha:0.75, time:_dragType == TimelineDataEvent.LIBRARY_DRAG ? 0.3 : 0, transition:Equations.easeNone});
		}
		
		public function move(xPos:Number, yPos:Number):void
		{
			x = xPos; y = yPos;
		}
		
		public function remove():void
		{
			_isActive = false;
			_hasData = false;
			
			Tweener.removeTweens(this);
			Tweener.addTween(this, {alpha:0, time:0.3, transition:Equations.easeNone, onComplete:parent.removeChild, onCompleteParams:[this]});	
			
			_dragType = null;
			
		}
	}
}