package com.cbc.ourgame.slideshowmaker
{
	import com.cbc.ourgame.slideshowmaker.controller.StandaloneStartup;
	import com.cbc.ourgame.slideshowmaker.controller.StartupCommand;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.StartupData;
	
	import org.puremvc.as3.multicore.patterns.facade.Facade;

	public class AppFacade extends Facade
	{
		public static const LARGE_BROKEN_IMG:String = "images/brokenLg.jpg";
		public static const SMALL_BROKEN_IMG:String = "images/brokenSm.jpg";
		
		public function AppFacade(key:String)
		{
			super(key);
		}
		
		public static function getInstance(key:String):AppFacade
		{
			if(instanceMap[key] == null) instanceMap[key] = new AppFacade(key);
			return instanceMap[key];
		}
		
		override protected function initializeController():void
		{
			super.initializeController();
			registerCommand(StandaloneStartup.STANDALONE_PLAYER, StandaloneStartup);
			registerCommand(StartupCommand.NAME, StartupCommand);
		}
		
		public function startup(startupData:StartupData):void
		{
			//test to see if standalone flag added to embed code
			var note:String = startupData.isStandAlone ? StandaloneStartup.STANDALONE_PLAYER : StartupCommand.NAME; 
			sendNotification(note, startupData);
		}
		
	}
}