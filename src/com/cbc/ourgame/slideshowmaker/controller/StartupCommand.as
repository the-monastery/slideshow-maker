package com.cbc.ourgame.slideshowmaker.controller
{
	import com.cbc.ourgame.slideshowmaker.components.RootPanel;
	import com.cbc.ourgame.slideshowmaker.components.audio.AudioPanel;
	import com.cbc.ourgame.slideshowmaker.components.editor.GalleryEditor;
	import com.cbc.ourgame.slideshowmaker.components.player.SlideshowPlayer;
	import com.cbc.ourgame.slideshowmaker.components.ui.FlyUpMenu;
	import com.cbc.ourgame.slideshowmaker.components.ui.FrameButton;
	import com.cbc.ourgame.slideshowmaker.model.GalleryProxy;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.StartupData;
	import com.cbc.ourgame.slideshowmaker.view.AudioPanelMediator;
	import com.cbc.ourgame.slideshowmaker.view.GalleryEditorMediator;
	import com.cbc.ourgame.slideshowmaker.view.MainToggleMediator;
	import com.cbc.ourgame.slideshowmaker.view.MusicPlayerMediator;
	import com.cbc.ourgame.slideshowmaker.view.RootPanelMediator;
	import com.cbc.ourgame.slideshowmaker.view.ScrollerMediator;
	import com.cbc.ourgame.slideshowmaker.view.SlideshowPlayerMediator;
	import com.cbc.ourgame.slideshowmaker.view.StageMediator;
	import com.cbc.ourgame.slideshowmaker.view.TimelineMediator;
	import com.ghostmonk.media.audio.MusicPlayer;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	public class StartupCommand extends SimpleCommand
	{
		public static const NAME:String = "StartupCommand";
		
		override public function execute(notification:INotification):void
		{	
			var startupData:StartupData = notification.getBody() as StartupData;
			var copy:XML = startupData.config.copy[0];
			
			facade.registerMediator(new StageMediator(startupData.stage));
			
			var rootPanel:RootPanel = new RootPanel();
			rootPanel.removeChild(rootPanel.standAloneTitle);
			rootPanel.removeChild(rootPanel.toggleCover);
				
			var galleryEditor:GalleryEditor = new GalleryEditor(		copy.galleryEditor[0], 
																		rootPanel.imageLibrary, 
																		rootPanel.editPreviewPanel, 
																		rootPanel.backBtn, 
																		rootPanel.galleryChooser);
																
			var slideShowPreview:SlideshowPlayer = new SlideshowPlayer(	rootPanel.previewScreen,
																		rootPanel.playBtn, 
																		rootPanel.previousSlide, 
																		rootPanel.shareBtn, 
																		rootPanel.volumeControl);
			
			//** 	Audio Panel creation 	**//
			var dropUpSelector:FlyUpMenu = new FlyUpMenu(rootPanel.audioSelector, startupData.config.audio[0]);															
			var audio:AudioPanel = new AudioPanel(dropUpSelector, rootPanel.editAudioToggle, rootPanel.smallVolumeControl);
			facade.registerMediator(new AudioPanelMediator(audio));
			//**							**//
			
			facade.registerProxy(new GalleryProxy());
			registerCommands();
			facade.registerMediator(new GalleryEditorMediator(galleryEditor));
			facade.registerMediator(new TimelineMediator(rootPanel.timeline));
			facade.registerMediator(new ScrollerMediator(rootPanel.scroller));
			facade.registerMediator(new SlideshowPlayerMediator(slideShowPreview));
			facade.registerMediator(new MainToggleMediator(new FrameButton(rootPanel.mainToggleBtn), copy.mainToggle[0]));
			facade.registerMediator(new RootPanelMediator(rootPanel, copy.header[0])); 
			facade.registerMediator(new MusicPlayerMediator(new MusicPlayer()));
		}
		
		private function registerCommands():void
		{
			facade.registerCommand(SubmitGalleryCommand.SUBMIT_PHOTOS, SubmitGalleryCommand);
			facade.registerCommand(UserGalleryCommand.GET_USER_GALLERIES, UserGalleryCommand);
		}		
	}
}