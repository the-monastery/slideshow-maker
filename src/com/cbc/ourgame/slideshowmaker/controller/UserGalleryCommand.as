package com.cbc.ourgame.slideshowmaker.controller
{
	import com.cbc.ourgame.slideshowmaker.model.GalleryProxy;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	public class UserGalleryCommand extends SimpleCommand
	{
		public static const GET_USER_GALLERIES:String = "getUserGalleries";
		
		override public function execute(note:INotification):void
		{
			GalleryProxy(facade.retrieveProxy(GalleryProxy.NAME)).getGallery();
		}
		
	}
}