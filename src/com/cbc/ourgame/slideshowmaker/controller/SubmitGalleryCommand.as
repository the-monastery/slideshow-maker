package com.cbc.ourgame.slideshowmaker.controller
{
	import com.cbc.ourgame.slideshowmaker.model.datastructures.PhotoData;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.UserCreatedGallery;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.Timer;
	
	import mx.utils.StringUtil;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	public class SubmitGalleryCommand extends SimpleCommand
	{
		public static const SUBMIT_PHOTOS:String = "submitPhotos";
		public static const GALLERY_KEY_RETURNED:String = "galleryKeyReturned";
		
		private const PLUCK_GET_ASSETS:String = "getAssets";
		private const PLUCK_CREATE_GALLERY:String = "submitGallery";
		private const PLUCK_UPDATE_PHOTO:String = "updatePhoto";
		private const URL:String = "proxy.php";
			
		private var _loader:URLLoader;
		private var _key:String;
		private var _photos:Array;
		private var _uploadCount:int;
		
		override public function execute(note:INotification):void
		{
			var galleryData:UserCreatedGallery = note.getBody() as UserCreatedGallery;
			
			_photos = galleryData.photos;
			
			_loader = new URLLoader();
			_loader.addEventListener(Event.COMPLETE, onPhotoUploadComplete);
			
			
			var timer:Timer = new Timer( 5000, 1 );
			timer.addEventListener( TimerEvent.TIMER_COMPLETE, updatePhotoMetadata );
			timer.start();
			
			//ExternalInterface.addCallback("setGalleryId", setGalleryKey);
			//ExternalInterface.call(PLUCK_CREATE_GALLERY, galleryData.name, "", "song_"+galleryData.song);
		}
		
		private function setGalleryKey(key:String) :void
		{
			_key = key;
			if(!Main.TESTING) beginUploadingPhotos();
		}
		
		public function beginUploadingPhotos() :void
		{
			_uploadCount = 0;
			uploadPhoto();
		}
		
		private function uploadPhoto() :void
		{
			var request:URLRequest = new URLRequest(URL);
			request.method = URLRequestMethod.POST;
			request.data = createUrlVariables(_key, _photos[_uploadCount].full);
			//ExternalInterface.call("console.log", "PHOTO_URL:"+_photos[_uploadCount].full);
			_loader.load(request);
		}
		
		private function onPhotoUploadComplete(e:Event):void
		{
			//ExternalInterface.call("console.log", "PHOTO_UPLOADED:"+e.target.data);
			var newKey:String = e.target.data.split("</script>")[1];
			newKey = StringUtil.trim(newKey);
			_photos[_uploadCount].key = newKey; 
			if (++_uploadCount < _photos.length)
			{	
				uploadPhoto();
			}
			else
			{ 
				updatePhotoMetadata();
			}
		}
		
		private function updatePhotoMetadata( e:Event = null ) :void
		{
			for each(var photo:PhotoData in _photos)
			{
				//ExternalInterface.call("console.log", "STILL EXISTS?: "+ photo.key);
				/*ExternalInterface.call(PLUCK_UPDATE_PHOTO, 
										photo.key, 
										photo.title,
										photo.description, 
										photo.tags);*/
			}			
			facade.sendNotification(GALLERY_KEY_RETURNED, _key);
		}
		
		private function createUrlVariables(key:String, url:String):URLVariables
		{
			var variables:URLVariables = new URLVariables();
			variables.gallery_key = key;
			variables.photo_url = url;
			return variables;
		}
		
	}
} 