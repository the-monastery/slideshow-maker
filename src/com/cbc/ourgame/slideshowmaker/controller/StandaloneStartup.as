package com.cbc.ourgame.slideshowmaker.controller
{
	import assets.RootPanelAsset;
	
	import com.cbc.ourgame.slideshowmaker.components.player.SlideshowPlayer;
	import com.cbc.ourgame.slideshowmaker.model.GalleryProxy;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.StartupData;
	import com.cbc.ourgame.slideshowmaker.view.MusicPlayerMediator;
	import com.cbc.ourgame.slideshowmaker.view.SlideshowPlayerMediator;
	import com.ghostmonk.media.audio.MusicPlayer;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	public class StandaloneStartup extends SimpleCommand
	{
		public static const STANDALONE_PLAYER:String = "standAlonePlayer";
		
		override public function execute(note:INotification):void
		{
			var startupData:StartupData = note.getBody() as StartupData;
			var copy:XML = startupData.config.copy[0];
			
			var music:XMLList = startupData.config.audio.track;
			
			var rootPanel:RootPanelAsset = new RootPanelAsset();
			
			startupData.stage.addChild(rootPanel);
			
			rootPanel.removeChild(rootPanel.audioSelector);
			rootPanel.removeChild(rootPanel.editAudioToggle);
			rootPanel.removeChild(rootPanel.header);
			rootPanel.removeChild(rootPanel.galleryChooser);
			rootPanel.removeChild(rootPanel.mainToggleBtn);
			rootPanel.removeChild(rootPanel.smallVolumeControl);
			rootPanel.removeChild(rootPanel.imageLibrary);
			rootPanel.removeChild(rootPanel.timelineItem01);
			rootPanel.removeChild(rootPanel.timelineItem02);
			rootPanel.removeChild(rootPanel.timelineItem03);
			rootPanel.removeChild(rootPanel.timelineItem04);
			rootPanel.removeChild(rootPanel.timelineItem05);
			rootPanel.removeChild(rootPanel.timelineItem06);
			rootPanel.removeChild(rootPanel.timelineItem07);
			rootPanel.removeChild(rootPanel.timelineItem08);
			rootPanel.removeChild(rootPanel.scrollBar);
			rootPanel.removeChild(rootPanel.editPreviewPanel);
			rootPanel.removeChild(rootPanel.backBtn);
			rootPanel.removeChild(rootPanel.editBorder);
			
			rootPanel.volumeControl.removeChild(rootPanel.volumeControl.volumeBorder);
			rootPanel.volumeControl.x = int(rootPanel.previewScreen.x + rootPanel.previewScreen.width - rootPanel.volumeControl.width - 7);
			
			var galleryProxy:GalleryProxy = new GalleryProxy(true, music);
			facade.registerProxy(galleryProxy);
			var slideShowPreview:SlideshowPlayer = new SlideshowPlayer(	rootPanel.previewScreen,
																		rootPanel.playBtn, 
																		rootPanel.previousSlide, 
																		rootPanel.shareBtn, 
																		rootPanel.volumeControl);
																
			slideShowPreview.isStandAlone(rootPanel.standAloneTitle);															
			slideShowPreview.show();
			rootPanel.removeChild(rootPanel.shareBtn);
			facade.registerMediator(new SlideshowPlayerMediator(slideShowPreview));
			facade.registerMediator(new MusicPlayerMediator(new MusicPlayer(), true));
			galleryProxy.getGallery();
		}
		
	}
}