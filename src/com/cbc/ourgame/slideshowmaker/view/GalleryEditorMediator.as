package com.cbc.ourgame.slideshowmaker.view
{
	import com.cbc.ourgame.slideshowmaker.components.editor.GalleryChooser;
	import com.cbc.ourgame.slideshowmaker.components.editor.GalleryEditor;
	import com.cbc.ourgame.slideshowmaker.components.thumblib.Library;
	import com.cbc.ourgame.slideshowmaker.components.thumblib.Thumbnail;
	import com.cbc.ourgame.slideshowmaker.events.GalleryViewEvent;
	import com.cbc.ourgame.slideshowmaker.events.MeasurementEvent;
	import com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent;
	import com.cbc.ourgame.slideshowmaker.model.GalleryProxy;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.TimelineData;
	
	import flash.events.MouseEvent;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class GalleryEditorMediator extends Mediator
	{
		public static const NAME:String = "GalleryEditorMediator";
		
		private var _library:Library;
		private var _chooser:GalleryChooser;
		private var _timelineItems:int;
		
		public function get galleryEditor():GalleryEditor
		{
			return viewComponent as GalleryEditor;
		}
		
		public function GalleryEditorMediator(viewComponent:GalleryEditor)
		{
			super(NAME, viewComponent);
			
			_library = galleryEditor.library;
			_chooser = galleryEditor.galleryChooser
			_timelineItems = 0;
			
			_chooser.addEventListener(MeasurementEvent.HEIGHT_MEASUREMENT, onHeightMeasure);
			_chooser.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			
			_library.addEventListener(MeasurementEvent.HEIGHT_MEASUREMENT, onHeightMeasure);
			_library.addEventListener(TimelineDataEvent.LIBRARY_DRAG, onLibraryDrag);
			_library.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			
			_library.view.addEventListener(MouseEvent.ROLL_OVER, onLibraryRollOver);
			_library.view.addEventListener(MouseEvent.ROLL_OUT, onLibraryRollOut);
			
			galleryEditor.previewPanel.addEventListener(TimelineDataEvent.PREVIEW_PANE_CLICK, onPreviewPaneClick);
			galleryEditor.previewPanel.addEventListener(TimelineDataEvent.LIBRARY_DRAG, onLibraryDrag);
			
			galleryEditor.addEventListener(GalleryViewEvent.GALLERY_CHOOSER, onGalleryEvent);
			galleryEditor.addEventListener(GalleryViewEvent.GALLERY_EDITOR, onGalleryEvent);
		}
		
		override public function listNotificationInterests():Array
		{
			return [
						GalleryProxy.GALLERY_PROXY_COMPLETE,
						MeasurementEvent.SCROLLER_PERCENT,
						MainToggleMediator.EDIT_MODE,
						MainToggleMediator.PREVIEW_MODE,
						TimelineDataEvent.TIMELINE_DRAG,
						TimelineMediator.NUMBER_OF_ITEMS
					];
		}
		
		override public function handleNotification(note:INotification):void
		{
			switch(note.getName())
			{
				case GalleryProxy.GALLERY_PROXY_COMPLETE: 	galleryProxyComplete(note); 	break;
				case MeasurementEvent.SCROLLER_PERCENT: 	onScroll(note); 				break;
				case MainToggleMediator.EDIT_MODE:  		galleryEditor.show();			break;
				case MainToggleMediator.PREVIEW_MODE:  		galleryEditor.hide(); 			break;
				case TimelineMediator.NUMBER_OF_ITEMS:  	trackTimelineLength(note); 			break;
				case TimelineDataEvent.TIMELINE_DRAG:  		galleryEditor.previewPanel.addPreview(note.getBody() as TimelineData); 			break;
			}
		}
		
		private function galleryProxyComplete(note:INotification):void
		{
			_chooser.addGalleryItems(note.getBody() as Array);
		}
		
		private function onHeightMeasure(e:MeasurementEvent):void
		{
			sendNotification(e.type, e.value);
			if(e.target == _library)	
				galleryEditor.previewPanel.addPreview(Thumbnail(_library.items[0]).timelineData);
		}
		
		private function onScroll(note:INotification):void
		{
			var percent:Number = note.getBody() as Number;
			_library.scroll(percent);
			_chooser.scroll(percent);
		}
		
		private function onLibraryDrag(e:TimelineDataEvent):void
		{
			galleryEditor.previewPanel.addPreview(e.timelineData);
			sendNotification(e.type, e.timelineData);
		}
		
		private function onGalleryEvent(e:GalleryViewEvent):void
		{
			sendNotification(e.type, galleryEditor.currentGalleryTitle);
			//calibrate the scroller when the gallery view is switched to the chooser view
			if(e.type == GalleryViewEvent.GALLERY_CHOOSER) 
			{
				sendNotification(MeasurementEvent.HEIGHT_MEASUREMENT, _chooser.view.height);
				_chooser.resetYPos();
			}
		}
		
		private function onMouseWheel(e:MouseEvent):void
		{
			sendNotification(e.type, e.delta*-2);
		}
		
		private function onPreviewPaneClick(e:TimelineDataEvent):void
		{
			sendNotification(e.type, e.timelineData);
		}
		
		private function onLibraryRollOut(e:MouseEvent):void
		{
			sendNotification(StageMediator.HIDE_TOOL_TIP);
		}
		
		private function onLibraryRollOver(e:MouseEvent):void
		{
			if(_timelineItems == 0) sendNotification(StageMediator.SHOW_TOOL_TIP);
		}
		
		private function trackTimelineLength(note:INotification):void
		{
			_timelineItems = note.getBody() as int;
		}
		
	}
}