package com.cbc.ourgame.slideshowmaker.view
{
	import com.cbc.ourgame.slideshowmaker.components.ui.Scroller;
	import com.cbc.ourgame.slideshowmaker.events.GalleryViewEvent;
	import com.cbc.ourgame.slideshowmaker.events.MeasurementEvent;
	
	import flash.events.MouseEvent;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class ScrollerMediator extends Mediator
	{
		public static const NAME:String = "ScrollerMediator";
		private var _homePosX:Number;
		
		public function get scroller():Scroller
		{
			return viewComponent as Scroller;
		}
		
		public function ScrollerMediator(viewComponent:Scroller)
		{
			super(NAME, viewComponent);
			_homePosX = viewComponent.view.x;
			viewComponent.addEventListener(MeasurementEvent.SCROLLER_PERCENT, onScroll);
		}
		
		override public function listNotificationInterests():Array
		{
			return [
						MeasurementEvent.HEIGHT_MEASUREMENT,
						GalleryViewEvent.GALLERY_CHOOSER,
						GalleryViewEvent.GALLERY_EDITOR,
						MouseEvent.MOUSE_WHEEL
					];
		}
		
		override public function handleNotification(note:INotification):void
		{
			switch(note.getName())
			{
				case MeasurementEvent.HEIGHT_MEASUREMENT: 	onLibraryHeight(note); 		break;
				case GalleryViewEvent.GALLERY_CHOOSER: 		positionScroller(note); 	break;
				case GalleryViewEvent.GALLERY_EDITOR: 		positionScroller(note); 	break;
				case MouseEvent.MOUSE_WHEEL: 				onMouseWheel(note); 		break;
			}
		}
		
		private function onMouseWheel(note:INotification):void
		{
			scroller.scroll(note.getBody() as Number);
		}
		
		private function onLibraryHeight(note:INotification):void
		{
			scroller.setHandle(note.getBody() as Number);	
		}
		
		private function onScroll(e:MeasurementEvent):void
		{
			sendNotification(e.type, e.value);
		}
		
		private function positionScroller(note:INotification):void
		{
			scroller.view.x = note.getName() == GalleryViewEvent.GALLERY_CHOOSER ? _homePosX : 290;
		}
		
	}
}