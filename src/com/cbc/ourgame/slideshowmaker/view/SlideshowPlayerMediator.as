package com.cbc.ourgame.slideshowmaker.view
{
	import com.cbc.ourgame.slideshowmaker.components.player.SlideshowPlayer;
	import com.cbc.ourgame.slideshowmaker.controller.SubmitGalleryCommand;
	import com.cbc.ourgame.slideshowmaker.events.SlideshowEvent;
	import com.cbc.ourgame.slideshowmaker.events.SubmitGalleryEvent;
	import com.cbc.ourgame.slideshowmaker.model.GalleryProxy;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.UserCreatedGallery;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class SlideshowPlayerMediator extends Mediator
	{
		public static var NAME:String = "SlideshowPlayerMediator";
		public static var PLAYER_VOLUME:String = "playerVolume";
		
		public function get player():SlideshowPlayer
		{
			return viewComponent as SlideshowPlayer;
		}
		
		public function SlideshowPlayerMediator(viewComponent:SlideshowPlayer)
		{
			super(NAME, viewComponent);
			player.addEventListener(SlideshowEvent.SLIDESHOW_START, onSlideShowEvent);
			player.addEventListener(SlideshowEvent.SLIDESHOW_PAUSE, onSlideShowEvent);
			player.addEventListener(SlideshowEvent.SLIDESHOW_END, onSlideShowEvent);
			player.addEventListener(SubmitGalleryEvent.SUBMIT_GALLERY_NAME, onPublishSlideShow);
			player.addEventListener(SlideshowEvent.SLIDESHOW_RESTART, onRestart);
			player.addEventListener(SlideshowEvent.AD_COMPLETE, onAdComplete);
			player.addEventListener(SlideshowEvent.AD_LOADED, onAdLoaded);
		}
		
		override public function onRegister():void
		{
		}
		
		override public function listNotificationInterests():Array
		{
			return [
					MainToggleMediator.EDIT_MODE,
					MainToggleMediator.PREVIEW_MODE,
					AudioPanelMediator.AUDIO_PANEL_VOLUME,
					TimelineMediator.TIMELINE_IMAGES_READY,
					SubmitGalleryCommand.GALLERY_KEY_RETURNED,
					GalleryProxy.STAND_ALONE_DATA_READY
					];
		}
		
		override public function handleNotification(note:INotification):void
		{
			switch(note.getName())
			{
				case MainToggleMediator.EDIT_MODE:  			hide();  											break;
				case MainToggleMediator.PREVIEW_MODE: 			show();	 											break;
				case TimelineMediator.TIMELINE_IMAGES_READY:	makeSlideShow(note);								break;
				case AudioPanelMediator.AUDIO_PANEL_VOLUME: 	player.volume = note.getBody() as Number;			break;
				case SubmitGalleryCommand.GALLERY_KEY_RETURNED: player.setReturnedKey(note.getBody() as String);	break;
				case GalleryProxy.STAND_ALONE_DATA_READY: 		standAloneData(note);								break;
			}
		}
		
		private function makeSlideShow(note:INotification):void
		{
			player.loadSlideShow(note.getBody() as Array);
		}
		
		private function show():void
		{
			player.show();
		}
		
		private function hide():void
		{
			player.hide();	
			sendNotification(PLAYER_VOLUME, player.volume);
		}
		
		private function onSlideShowEvent(e:SlideshowEvent):void
		{
			sendNotification(e.type);
		}
		
		private function onPublishSlideShow(e:SubmitGalleryEvent):void
		{
			var photos:Array = TimelineMediator(facade.retrieveMediator(TimelineMediator.NAME)).timeline.data;
			var trackID:String = MusicPlayerMediator(facade.retrieveMediator(MusicPlayerMediator.NAME)).trackID;
			var userCreatedGallery:UserCreatedGallery = new UserCreatedGallery(e.name, trackID, photos); 
			sendNotification(SubmitGalleryCommand.SUBMIT_PHOTOS, userCreatedGallery);
		}
		
		private function standAloneData(note:INotification):void
		{
			var song:String = note.getBody().track;
			player.standAloneTitleField.htmlText = "<b>"+note.getBody().title+"</b>";
			player.loadSlideShow(note.getBody().photos);
		}
		
		private function onRestart(e:SlideshowEvent):void
		{
			sendNotification(e.type);
		}
		
		private function onAdLoaded(e:SlideshowEvent):void
		{
			sendNotification(e.type);
		}
		
		private function onAdComplete(e:SlideshowEvent):void
		{
			sendNotification(e.type);
		}
	}
}