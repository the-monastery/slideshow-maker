package com.cbc.ourgame.slideshowmaker.view
{
	import com.cbc.ourgame.slideshowmaker.components.ui.FrameButton;
	import com.cbc.ourgame.slideshowmaker.events.SlideshowEvent;
	
	import flash.events.MouseEvent;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class MainToggleMediator extends Mediator
	{
		public static const NAME:String = "MainToggleMediator";
		public static const EDIT_MODE:String = "editMode";
		public static const PREVIEW_MODE:String = "previewMode";
		
		private var _editMode:Boolean;
		private var _previewLabel:String;
		private var _editLabel:String;
		
		public function get toggle():FrameButton		
		{
			return viewComponent as FrameButton;
		}
		
		public function MainToggleMediator(viewComponent:FrameButton, copy:XML)
		{
			super(NAME, viewComponent);
			toggle.disable(true, true);
			_previewLabel = copy.preview.@en.toString();
			_editLabel = copy.edit.@en.toString();
		}
		
		override public function onRegister():void
		{
			_editMode = true;
			sendNotification(EDIT_MODE);
			toggle.addCallback(onMainToggle);	
		}
		
		private function onMainToggle(e:MouseEvent):void
		{
			_editMode = !_editMode;
			var mode:String = _editMode ? EDIT_MODE : PREVIEW_MODE; 
			toggle.view.label.text = _editMode ? _previewLabel : _editLabel;
			sendNotification(mode);
		}
		
		override public function listNotificationInterests():Array
		{
			return [ 	TimelineMediator.NUMBER_OF_ITEMS,
						SlideshowEvent.AD_COMPLETE,
						SlideshowEvent.AD_LOADED
						 ];	
		}
		
		override public function handleNotification(note:INotification):void
		{
			switch(note.getName())
			{
				case TimelineMediator.NUMBER_OF_ITEMS: 	setToggle(note); 		break;
				case SlideshowEvent.AD_COMPLETE: 		toggle.enable(); 		break;
				case SlideshowEvent.AD_LOADED: 			toggle.disable(true); 	break; 
			}
		}
		
		private function setToggle(note:INotification):void
		{
			if(note.getBody() as int >= 3)
				toggle.enable()
			else
				toggle.disable(true, true);	
		}
		
	}
}