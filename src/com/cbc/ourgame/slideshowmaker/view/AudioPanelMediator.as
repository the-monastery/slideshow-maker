package com.cbc.ourgame.slideshowmaker.view
{
	import com.cbc.ourgame.slideshowmaker.components.audio.AudioPanel;
	import com.cbc.ourgame.slideshowmaker.events.AudioSelectionEvent;
	import com.cbc.ourgame.slideshowmaker.events.SlideshowEvent;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class AudioPanelMediator extends Mediator
	{
		public static const NAME:String = "AudioPanelMediator";
		
		public static const AUDIO_PANEL_VOLUME:String = "audioPanelVolume";
		public static const AUDIO_PANEL_PLAY_CONTROL:String = "audioPanelPlayControl";
		
		public function get audioPanel():AudioPanel
		{
			return viewComponent as AudioPanel;
		}
		
		public function AudioPanelMediator(viewComponent:AudioPanel)
		{
			super(NAME, viewComponent);
			audioPanel.addPlayCallback(onPlayControl);
			audioPanel.addEventListener(AudioSelectionEvent.AUDIO_SELECTION, onAudioSelection);
		}
		
		override public function listNotificationInterests():Array
		{
			return [
						MainToggleMediator.EDIT_MODE,
						MainToggleMediator.PREVIEW_MODE,
						SlideshowPlayerMediator.PLAYER_VOLUME
					];
		}
		
		override public function handleNotification(note:INotification):void
		{
			switch(note.getName())
			{
				case MainToggleMediator.EDIT_MODE: 				show();											break;
				case MainToggleMediator.PREVIEW_MODE: 			hide();											break;
				case SlideshowPlayerMediator.PLAYER_VOLUME:		audioPanel.volume = note.getBody() as Number; 	break;
			}
		}
		
		public function show():void
		{
			audioPanel.show();
		}
		
		public function hide():void
		{
			audioPanel.hide();
			sendNotification(AUDIO_PANEL_VOLUME, audioPanel.volume);
		}
		
		private function onAudioSelection(e:AudioSelectionEvent):void
		{
			sendNotification(e.type, {request:e.request, id:e.id});
		}
		
		private function onPlayControl(isPlaying:Boolean):void
		{
			sendNotification(AUDIO_PANEL_PLAY_CONTROL, isPlaying);
		}
		
	}
}