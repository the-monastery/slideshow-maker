package com.cbc.ourgame.slideshowmaker.view
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.SoundShortcuts;
	
	import com.cbc.ourgame.slideshowmaker.components.audio.AudioPanel;
	import com.cbc.ourgame.slideshowmaker.events.AudioSelectionEvent;
	import com.cbc.ourgame.slideshowmaker.events.SlideshowEvent;
	import com.cbc.ourgame.slideshowmaker.model.GalleryProxy;
	import com.ghostmonk.media.audio.MusicPlayer;
	
	import flash.net.URLRequest;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class MusicPlayerMediator extends Mediator
	{
		public static const NAME:String = "MusicPlayerMediator";
		private var _currentSong:URLRequest;
		private var _currentVolume:Number;
		private var _trackID:String;
		private var _isStandAlone:Boolean;
		private var _isPlaying:Boolean;
		
		public function get trackID():String { return _trackID; }
		
		public function get musicPlayer():MusicPlayer
		{
			return viewComponent as MusicPlayer;
		}
		
		public function MusicPlayerMediator(viewComponent:MusicPlayer, isStandAlone:Boolean = false)
		{
			_isPlaying = false;
			_isStandAlone = isStandAlone;
			SoundShortcuts.init();
			super(NAME, viewComponent);
		}
		
		override public function onRegister():void
		{
			if(!_isStandAlone)
			{
				var panel:AudioPanel = AudioPanelMediator(facade.retrieveMediator(AudioPanelMediator.NAME)).audioPanel; 
				_currentSong = panel.currentTrack;
				_trackID = panel.trackID;
				_currentVolume = panel.volume;
				musicPlayer.load(_currentSong, false);
			}
		}
		
		override public function listNotificationInterests():Array
		{
			return [	
						AudioSelectionEvent.AUDIO_SELECTION,
						AudioPanelMediator.AUDIO_PANEL_PLAY_CONTROL,
						AudioPanelMediator.AUDIO_PANEL_VOLUME,
						SlideshowEvent.SLIDESHOW_END,
						SlideshowEvent.SLIDESHOW_START,
						SlideshowEvent.SLIDESHOW_PAUSE,
						SlideshowEvent.SLIDESHOW_RESUME,
						SlideshowEvent.SLIDESHOW_RESTART,
						GalleryProxy.STAND_ALONE_DATA_READY
					];
		}
		
		override public function handleNotification(note:INotification):void
		{
			switch(note.getName())
			{
				case AudioSelectionEvent.AUDIO_SELECTION: 			onAudioSelection(note);							break;
				case AudioPanelMediator.AUDIO_PANEL_PLAY_CONTROL: 	onAudioPanelToggle(note.getBody() as Boolean); 	break;
				case AudioPanelMediator.AUDIO_PANEL_VOLUME: 		_currentVolume = note.getBody() as Number;		break;
				case SlideshowEvent.SLIDESHOW_END:					onEnd();										break;
				case SlideshowEvent.SLIDESHOW_PAUSE:				onPause();										break;
				case SlideshowEvent.SLIDESHOW_START:				onStart();										break;
				case SlideshowEvent.SLIDESHOW_RESUME:				onResume();										break;
				case GalleryProxy.STAND_ALONE_DATA_READY:			onStandAlone(note);								break;
				case SlideshowEvent.SLIDESHOW_RESTART:				onEnd(); break;
			}
		}
		
		private function onAudioSelection(note:INotification):void
		{
			_currentSong = note.getBody().request;
			_trackID = note.getBody().id;
			musicPlayer.load(_currentSong, _isPlaying);	
		}
		
		private function onAudioPanelToggle(isPlaying:Boolean):void
		{
			_isPlaying = isPlaying;
			if(!isPlaying) onEnd();
			else onStart();
		}
		
		private function onEnd():void
		{
			Tweener.addTween(musicPlayer.channel, {_sound_volume:0, time:0.5, transition:Equations.easeNone, onComplete:musicPlayer.stop});
		}
		
		private function onPause():void
		{
			musicPlayer.pause();
		}
		
		private function onStart():void
		{
			Tweener.removeTweens(musicPlayer.channel);
			musicPlayer.play();
			//Tweener.addTween(musicPlayer.channel, {_sound_volume:1, time:0.5, transition:Equations.easeNone});
		}
		
		private function onResume():void
		{
			musicPlayer.play();
		}
		
		private function onStandAlone(note:INotification):void
		{
			musicPlayer.load(new URLRequest(note.getBody().track), false);
		}
		
	}
}