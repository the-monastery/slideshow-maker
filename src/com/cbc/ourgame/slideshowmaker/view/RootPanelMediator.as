/**
 * Written By: Nicholas Hillier, Jan 16, 2009
 * Controls the and mediates actions and events important to displaying the EditPanel
 */
package com.cbc.ourgame.slideshowmaker.view
{
 	import com.cbc.ourgame.slideshowmaker.components.RootPanel;
 	import com.cbc.ourgame.slideshowmaker.controller.UserGalleryCommand;
 	import com.cbc.ourgame.slideshowmaker.events.GalleryViewEvent;
 	import com.cbc.ourgame.slideshowmaker.model.GalleryProxy;
 	
 	import org.puremvc.as3.multicore.interfaces.INotification;
 	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class RootPanelMediator extends Mediator
	{
		public static const NAME:String = "RootPanelMediator";
		
		private var _userName:String;
		private var _copy:XML;
		
		public function get rootPanel():RootPanel
		{
			return viewComponent as RootPanel;
		}
		
		/**
		 * 
		 * @param viewComponent - a composed object holding 
		 * 
		 */
		public function RootPanelMediator(viewComponent:RootPanel, copy:XML)
		{
			_copy = copy;
			super(NAME, viewComponent);
			 
		}
		
		public function set rootPanelTitle(value:String):void
		{
			rootPanel.setTitle(value);
		}
		
		override public function onRegister():void
		{
			StageMediator(facade.retrieveMediator(StageMediator.NAME)).stage.addChild(rootPanel);
			sendNotification(UserGalleryCommand.GET_USER_GALLERIES);
		}
		
		override public function listNotificationInterests():Array
		{
			return [
						GalleryProxy.GALLERY_USER_NAME,
						MainToggleMediator.EDIT_MODE,
						MainToggleMediator.PREVIEW_MODE,
						GalleryViewEvent.GALLERY_EDITOR,
						GalleryViewEvent.GALLERY_CHOOSER
					];
		}
		
		override public function handleNotification(note:INotification):void
		{
			switch(note.getName())
			{
				case GalleryProxy.GALLERY_USER_NAME: 		getUserName(note); 									break;
				case MainToggleMediator.EDIT_MODE:  		rootPanel.show(); 									break;
				case MainToggleMediator.PREVIEW_MODE:  		rootPanel.hide(); 									break;
				case GalleryViewEvent.GALLERY_EDITOR:  		rootPanel.setTitle(note.getBody() as String); 		break;
				case GalleryViewEvent.GALLERY_CHOOSER:  	rootPanel.setTitle(_userName+_copy.user.@en); 		break;
			}
		}
		
		private function getUserName(note:INotification):void
		{
			_userName = note.getBody() as String;
			rootPanel.setTitle(_userName+_copy.user.@en);
		}
		
	}
}