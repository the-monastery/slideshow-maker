package com.cbc.ourgame.slideshowmaker.view
{
	import com.cbc.ourgame.slideshowmaker.components.timeline.Timeline;
	import com.cbc.ourgame.slideshowmaker.events.TimelineDataEvent;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.TimelineData;
	
	import flash.display.Sprite;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	public class TimelineMediator extends Mediator
	{
		public static const NAME:String = "TimelineMediator";
		public static const TIMELINE_IMAGES_READY:String = "timelineImagesReady";
		public static const NUMBER_OF_ITEMS:String = "numberOfItems"; 
		
		private var _createdGallery:Array;
		
		public function get timeline():Timeline { return viewComponent as Timeline;	}
		
		public function TimelineMediator(viewComponent:Timeline)
		{
			super(NAME, viewComponent);
			timeline.addEventListener(TimelineDataEvent.TIMELINE_DRAG, onTimelineDrag);
		}
		
		override public function listNotificationInterests():Array
		{
			return [ 	StageMediator.DRAG_TEST,
						StageMediator.DROP_TEST,
						StageMediator.DELETE_PRESSED,
						MainToggleMediator.PREVIEW_MODE,
						TimelineDataEvent.PREVIEW_PANE_CLICK,
						TimelineDataEvent.LIBRARY_DRAG
						 ];
		}
		
		override public function handleNotification(note:INotification):void
		{
			switch(note.getName())
			{
				case StageMediator.DROP_TEST: 				onDropTest(note); 						break;
				case StageMediator.DRAG_TEST: 				onDragTest(note); 						break;
				case StageMediator.DELETE_PRESSED: 			onDelete();								break;
				case MainToggleMediator.PREVIEW_MODE: 		onPreviewMode();	 					break;
				case TimelineDataEvent.LIBRARY_DRAG:		timeline.dragType = note.getName(); 	break;
				//case TimelineDataEvent.PREVIEW_PANE_CLICK: 	onPreviewPaneClick(note);	break;
			}
		}
		
		private function onDelete():void
		{
			timeline.deleteCurrent();
			sendNotification(NUMBER_OF_ITEMS, timeline.data.length); 
		}
		
		private function onDragTest(note:INotification):void
		{
			timeline.dragTest(Sprite(note.getBody()));
		}
		
		private function onDropTest(note:INotification):void
		{
			timeline.dropTest(note.getBody() as TimelineData);
			sendNotification(NUMBER_OF_ITEMS, timeline.data.length);
		}
		
		private function onPreviewMode():void
		{
			_createdGallery = timeline.data;
			sendNotification(TIMELINE_IMAGES_READY, _createdGallery);
		}
		
		private function onTimelineDrag(e:TimelineDataEvent):void
		{
			timeline.dragType = e.type;
			sendNotification(e.type, e.timelineData);
		}
		
		/* private function onPreviewPaneClick(note:INotification):void
		{
			timeline.addPhotoAutomatically(note.getBody() as TimelineData);
		} */
		
	}
}