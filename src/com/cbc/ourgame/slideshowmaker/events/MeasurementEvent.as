package com.cbc.ourgame.slideshowmaker.events
{
	import flash.events.Event;

	public class MeasurementEvent extends Event
	{
		public static const HEIGHT_MEASUREMENT:String = "heightMeasurement";
		public static const SCROLLER_PERCENT:String = "scrollerPercent";
		
		private var _value:Number;
		
		public function get value():Number { return _value; }
		
		public function MeasurementEvent(type:String, value:Number, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_value = value;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new MeasurementEvent(type, value);
		}
		
	}
}