package com.cbc.ourgame.slideshowmaker.events
{
	import flash.events.Event;

	public class GalleryViewEvent extends Event
	{
		public static const GALLERY_CHOOSER:String = "galleryChooser";
		public static const GALLERY_EDITOR:String = "galleryEditor";
		
		public function GalleryViewEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new GalleryViewEvent(type);
		}
		
	}
}