package com.cbc.ourgame.slideshowmaker.events
{
	import flash.events.Event;

	public class SlideshowEvent extends Event
	{
		public static const SLIDESHOW_START:String = "slideshowStart";
		public static const SLIDESHOW_PAUSE:String = "slideshowPause";
		public static const SLIDESHOW_RESUME:String = "slideshowResume";
		public static const SLIDESHOW_RESTART:String = "slideshowRestart";
		public static const SLIDESHOW_END:String = "slideshowEnd";
		public static const SLIDE_COMPLETE:String = "slideAnimComplete";
		public static const AD_LOADED:String = "adLoaded";
		public static const AD_COMPLETE:String = "adComplete";
		
		public function SlideshowEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new SlideshowEvent(type);
		}
		
	}
}