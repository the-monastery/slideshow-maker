package com.cbc.ourgame.slideshowmaker.events
{
	import flash.events.Event;
	import flash.net.URLRequest;

	public class AudioSelectionEvent extends Event
	{
		public static const AUDIO_SELECTION:String = "audioSelection";
		
		private var _request:URLRequest;
		private var _id:String;
		
		public function get id():String
		{
			return _id;
		}
		
		public function get request():URLRequest
		{
			return _request;
		}
		
		public function AudioSelectionEvent(type:String, request:URLRequest, id:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_request = request;
			_id = id;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new AudioSelectionEvent(type, request, id);
		}
		
	}
}