package com.cbc.ourgame.slideshowmaker.events
{
	import flash.events.Event;

	public class SubmitGalleryEvent extends Event
	{
		public static const SUBMIT_GALLERY_NAME:String = "submitGalleryName";
		
		private var _name:String;
		
		public function get name():String
		{
			return _name; 
		}
		
		public function SubmitGalleryEvent(type:String, name:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_name = name;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new SubmitGalleryEvent(type, name);
		}
		
	}
}