package com.cbc.ourgame.slideshowmaker.events
{
	import com.cbc.ourgame.slideshowmaker.model.datastructures.TimelineData;
	
	import flash.events.Event;

	public class TimelineDataEvent extends Event
	{
		public static const LIBRARY_DRAG:String = "libraryDrag";
		public static const TIMELINE_DRAG:String = "timeLineDrag";
		public static const PREVIEW_PANE_CLICK:String = "previewPaneClick";
		
		private var _timelineData:TimelineData;
		
		public function get timelineData():TimelineData { return _timelineData; } 
		
		public function TimelineDataEvent(type:String, timelineData:TimelineData, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			_timelineData = timelineData
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new TimelineDataEvent(type, timelineData);
		}
		
	}
}