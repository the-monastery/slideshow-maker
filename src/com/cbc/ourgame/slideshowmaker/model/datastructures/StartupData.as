package com.cbc.ourgame.slideshowmaker.model.datastructures
{
	import flash.display.Sprite;
	import flash.display.Stage;
	
	public class StartupData
	{
		private var _stage:Stage;
		private var _galleryID:int;
		private var _config:XML;
		private var _mainApp:Sprite;
		private var _isStandAlone:Boolean;
		
		public function get stage():Stage {	return _stage; }
		public function get galleryID():int { return _galleryID; }
		public function get config():XML { return _config; }
		public function get mainApp():Sprite { return _mainApp; }
		public function get isStandAlone():Boolean { return _isStandAlone; }
		
		public function StartupData(stage:Stage, galleryID:int, config:XML, mainApp:Sprite, isStandAlone:Boolean)
		{
			_stage = stage;
			_galleryID = galleryID;
			_config = config;
			_mainApp = mainApp;
			_isStandAlone = isStandAlone;
		}

	}
}