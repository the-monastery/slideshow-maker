package com.cbc.ourgame.slideshowmaker.model.datastructures
{
	public class PhotoData
	{
		private var _small:String;
		private var _large:String;
		private var _full:String;
		private var _title:String;
		private var _medium:String;
		private var _description:String;
		//private var _tags:String;
		private var _key:String;
		
		public function get small():String		{ return _small;}
		public function get large():String		{ return _large;}
		public function get medium():String		{ return _medium; }
		public function get full():String		{ return _full;}
		public function get title():String		{ return _title;}
		public function get description():String{ return _description;}
		//public function get tags():String		{ return _tags;}
		//public function get key():String 		{ return _key;}
		
		public function set key(value:String):void	{ _key = value; }
				
		public function PhotoData(item:Object)
		{
			_small = item.Image.Small;
			_large = item.Image.Large;
			_full = item.Image.Full;
			_title = item.Title;
			_medium = item.Image.Medium;
			_description = item.Description;
			//_tags = item.Tags;
			//_key = item.GalleryKey.Key;
		}
		
		public function toString():String
		{
			return 	"Small: "+_small+"\n"+
					"Large: "+_large+"\n"+
					"Title: "+_title+"\n"+
					"Decription: "+_description+"\n";
					//"Tags: "+_tags+"\n";
		}

	}
}