package com.cbc.ourgame.slideshowmaker.model.datastructures
{
	public class UserCreatedGallery
	{
		private var _name:String;
		private var _song:String;
		private var _photos:Array;
		
		public function get name():String 		{ 	return _name; 		}
		public function get song():String 		{ 	return _song; 		}
		public function get photos():Array 		{ 	return _photos; 	}
		
		public function UserCreatedGallery(name:String, song:String, photos:Array)
		{
			_name = name;
			_song = song;
			_photos = photos;
		}
		
		public function toString():String
		{
			var photoURLs:String = "";
			
			for each(var photo:PhotoData in _photos)
			{	
				photoURLs += photo.full+"\n";
			}
				
			return 	"Name: "+_name+
					"Song: "+_song+
					"Photos: \n"+photoURLs;
		}
		
	}
}