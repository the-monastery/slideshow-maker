package com.cbc.ourgame.slideshowmaker.model.datastructures
{
	import flash.display.BitmapData;
	
	public class TimelineData
	{
		private var _bitmapData:BitmapData;
		private var _photoData:PhotoData;
		
		public function get bitmapData():BitmapData { return _bitmapData; }
		public function get photoData():PhotoData { return _photoData; }
		
		public function set bitmapData(value:BitmapData):void { _bitmapData = value;}
		
		public function TimelineData(metaData:PhotoData, bitmapData:BitmapData = null)
		{
			_bitmapData = bitmapData;
			_photoData = metaData;
		}
	}
}