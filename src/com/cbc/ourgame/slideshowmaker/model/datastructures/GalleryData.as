package com.cbc.ourgame.slideshowmaker.model.datastructures
{
	public class GalleryData
	{
		//private var _createdOn:String;
		private var _description:String;
		//private var _galleryKey:String;
		private var _photos:Array;
		//private var _tags:String;
		private var _title:String;
		private var _photoURL:String;
		
		//public function get date():String 			{ 	return _createdOn; 		}
		public function get description():String 	{ 	return _description; 	}
		//public function get galleryKey():String 	{ 	return _galleryKey; 	}
		public function get photos():Array 			{ 	return _photos;			}		
		//public function get tags():String 			{	return _tags;			}
		public function get title():String 			{ 	return _title;			}
		public function get photoURL():String 		{ 	return _photoURL;		}
		
		public function GalleryData(galleryObject:Object)
		{
			//_createdOn = galleryObject.CreatedOn;
			_description = galleryObject.Description;
			//_galleryKey = galleryObject.GalleryKey;
			createPhotos(galleryObject.Photos);
			//_tags = galleryObject.Tags;
			_title = galleryObject.Title;
			_photoURL = galleryObject.PhotoUrl;
			
			galleryObject = null;
		}
		
		private function createPhotos(photos:Array):void
		{
			_photos = [];
			for each(var item:Object in photos)
				_photos.push(new PhotoData(item));
		}
		
		public function toString():String
		{
			return  "Description: "+_description+"\n"+
					//"Created On: "+_createdOn+"\n"+
					//"GalleryKey: "+_galleryKey+"\n"+
					"Photos: "+_photos.toString()+"\n"+
					//"Tags: "+_tags+"\n"+
					"Title: "+_title+"\n";
		}

	}
}