/**
 * Written By: Nicholas Hillier Jan 15 2009
 * Makes a call to external interface, and dispatches an event when data has been returned
 */
package com.cbc.ourgame.slideshowmaker.model
{
	import com.cbc.ourgame.slideshowmaker.model.datastructures.GalleryData;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.PhotoData;
	
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.utils.Timer;
	
	import org.puremvc.as3.multicore.patterns.proxy.Proxy;

	public class GalleryProxy extends Proxy
	{
		public static const NAME:String = "GalleryProxy";
		public static const GALLERY_PROXY_COMPLETE:String = "galleryProxyComplete";
		public static const GALLERY_USER_NAME:String = "gelleryUserName";
		public static const STAND_ALONE_DATA_READY:String = "standAloneDdataReady";
	
		private const GET_GALLERIES:String ="getUserAssets";
		private const GET_STAND_ALONE_DATA:String = "getUserGallery";
		private var _music:XMLList;
		private var _isStandAlone:Boolean;
		
		public function GalleryProxy(isStandAlone:Boolean = false, music:XMLList = null)
		{
			_isStandAlone = isStandAlone;
			if(music) _music = music;
			super(NAME);
		}
		
		public function getGallery():void
		{
			
			if(ExternalInterface.available)
			{
				try
				{
					var call:String = _isStandAlone ? GET_STAND_ALONE_DATA : GET_GALLERIES;
					var items:Object = ExternalInterface.call(call);
					if(items)
					{
						_isStandAlone ? standAloneDataReady(items) : galleriesReady(items);
					}
					else
					{
						var readyTimer:Timer = new Timer(100, 0);
						readyTimer.addEventListener(TimerEvent.TIMER, onTimer);
						readyTimer.start();
					}
				}
				catch(e:Error)
				{
					trace(e.message);
				}
			}
			else
			{
				trace("External Interface not ready");
			}
		}
		 
		private function onTimer(e:TimerEvent):void
		{
			Timer(e.target).stop();
			getGallery();
		}
		
		private function standAloneDataReady(gallery:Object):void
		{
			var list:Array = [];
			
			for each(var photo:Object in gallery.Photos)
			{
				list.push(new PhotoData(photo));
			}
			
			//"song_breathe,sports-hockey-grassroots,Slideshow_FLA"
			
			var test:RegExp = /song_[a-zA-Z]*/g;
			
			var tags:String = gallery.Tags;
			var tag:String = tags.match(test)[0];
			var song:String = tag.replace("song_", "");
			
			var mTrack:String;
			for each(var item:XML in _music)
			{
				if(item.@id.toString() == song)
					mTrack = item.@src;
			}
			sendNotification(STAND_ALONE_DATA_READY, {photos:list, title:gallery.Title, track:mTrack});
		}
		
		private function galleriesReady(galleries:Object):void
		{
			var list:Array = [];
			var userName:String;
			
			for each(var gallery:Object in galleries)
			{ 
				if(gallery.Photos.length > 0)
				{
					list.push(new GalleryData(gallery));
					
					try	{userName = gallery.Photos[0].Author.DisplayName;}
					catch(e:Error){}
				}
			}
			
			if(userName == "" || userName == null) userName = " ";
			
			sendNotification(GALLERY_USER_NAME, userName); 
			sendNotification(GALLERY_PROXY_COMPLETE, list);
		}
		
	}
}