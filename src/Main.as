package
{
	import caurina.transitions.properties.ColorShortcuts;
	import caurina.transitions.properties.DisplayShortcuts;
	import caurina.transitions.properties.TextShortcuts;
	
	import com.cbc.ourgame.slideshowmaker.AppFacade;
	import com.cbc.ourgame.slideshowmaker.model.datastructures.StartupData;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	[SWF(backgroundColor=0xffffff, frameRate=31, width=593, height=469, pageTitle="Slideshow Maker")]
	
	public class Main extends Sprite
	{
		public static const NAME:String = "Main";
		public static const TESTING:Boolean = true;

		private var _configLoader:URLLoader;
		
		public function Main()
		{	
			DisplayShortcuts.init();
			TextShortcuts.init();
			ColorShortcuts.init();
			
			var configURL:String = TESTING ? "xmlData/config.xml" : "xmlData/config.xml";
			_configLoader = new URLLoader(new URLRequest(configURL));
			_configLoader.addEventListener(Event.COMPLETE, configReady);
		}
		
		private function configReady(e:Event):void
		{
			var galID:int = root.loaderInfo.parameters.photoGalID;
			var standAlone:Boolean = root.loaderInfo.parameters.player;
			var startupData:StartupData = new StartupData(stage, galID, XML(e.target.data), this, standAlone);
			AppFacade.getInstance(NAME).startup(startupData);
		}
	}
}