/**
 * Written by: Nicholas Hillier Nov 22, 2008
 * 
 * This class composits movieclips with button behaviour
 * Tweener must be included for operation
 * frame 1 is mouseOut, totalframes is mouseover
 * All effects (transitions etc) are to be created on the timeline as linear transitions are used to move the playhead 
 * 
 * @param clip:MovieClip 
 */
package com.ghostmonk.ui
{
	import caurina.transitions.*;
	
	import com.ghostmonk.ui.idecomposed.ClickActionButton;
	
	import flash.display.*;
	
	public class MovieClipButton extends Sprite
	{
		private var _clip:ClickActionButton;
		
		public function MovieClipButton(clip:MovieClip, callback:Function)
		{
			_clip = new ClickActionButton(clip, callback);
			
			addChild(_clip.view);
			
			enable();
		}
		
		public function enable():void
		{
			_clip.enable();
		}
		
		public function disable():void
		{
			_clip.disable();
		}

	}
}