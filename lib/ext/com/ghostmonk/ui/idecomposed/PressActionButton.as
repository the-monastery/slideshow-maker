/**
 * Written by: Nicholas Hillier Dec 2008
 * MouseDown behaviour added to a movieclip created in the Flash IDE
 * The clip will be composed into a new FrameLabelButton so the interactive 
 * functionality of that class/object is used.
 * 
 * @param clip:MovieClip 
 * @param callback:Function
 * 
 * callback function is continuously called onMouseDown, and the event is cloned and passed into the callback as a parameter  
*/
package com.ghostmonk.ui.idecomposed
{
	import com.ghostmonk.ui.FrameLabelButton;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class PressActionButton
	{
		private var _clip:FrameLabelButton;
		private var _callBack:Function;
		
		public function get view():MovieClip
		{
			return _clip.view;
		}
		
		public function PressActionButton(clip:MovieClip, callBack:Function)
		{
			_clip = new FrameLabelButton(clip);
			_callBack = callBack;
				
			enable();
		}
		
		public function enable():void
		{
			_clip.enable();
			_clip.addEventListener(MouseEvent.MOUSE_DOWN, onPress);
			_clip.addEventListener(MouseEvent.MOUSE_UP, onDepress);
			_clip.addEventListener(MouseEvent.ROLL_OUT, onDepress);
		}
		
		public function disable():void
		{
			_clip.disable();
			_clip.removeEventListener(MouseEvent.MOUSE_DOWN, onPress);
			_clip.removeEventListener(MouseEvent.MOUSE_UP, onDepress);
			_clip.removeEventListener(MouseEvent.ROLL_OUT, onDepress);
		}
		
		private function onPress(e:Event):void
		{
			_clip.view.addEventListener(Event.ENTER_FRAME, onPress);
			_callBack(e);
		}
		
		private function onDepress(e:Event):void
		{
			_clip.view.removeEventListener(Event.ENTER_FRAME, onPress);	
		}

	}
}