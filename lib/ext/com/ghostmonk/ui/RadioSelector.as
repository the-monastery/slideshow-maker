/**
*
 * Written by Nicholas Hillier Nov 2008 
*/
package com.ghostmonk.ui
{
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	
	public class RadioSelector extends Sprite
	{
		private var _isChecked:Boolean;
		private var _node:int;
		
		public function RadioSelector(node:int)
		{
			_node = node;
			//this.filters = [Config.DROP_SHADOW];
			enable();
		}
		
		public function enable():void
		{
			draw();
			_isChecked = false;
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		public function disable():void
		{
			buttonMode = false;
			removeEventListener(MouseEvent.CLICK, onMouseClick);
		}
		
		private function onMouseClick(e:MouseEvent):void
		{
			draw(true);
			_isChecked = true;
			disable();
			//dispatchEvent(new RadioSelectionEvent(RadioSelectionEvent.RADIO_SELECTION, _node));
		}
		
		private function draw(selected:Boolean = false):void
		{
			graphics.clear();
			graphics.lineStyle(1,0x686868);
			graphics.beginFill(0xFFFFFF);
			graphics.drawCircle(0,0,7.5);
			if(selected)
			{
				graphics.beginFill(0x686868);
				graphics.drawCircle(0,0,4.5);
			}
			graphics.endFill();
		}

	}
}