package com.ghostmonk.ui.video
{
	import com.ghostmonk.events.video.VideoLoadEvent;
	import com.ghostmonk.ui.TextButton;
	
	import flash.events.MouseEvent;

	[Event(name="loadVideo", type="com.ghostmonk.events.VideoLoadEvent")]

	public class PlaylistLink extends TextButton
	{
		private var _url:String;
		
		public function PlaylistLink(font:String, textSize:Number, label:String, url:String, textColor:uint, baseColor:uint, width:Number)
		{
			super(font, textSize, label, textColor, baseColor, width);
			_url = url;
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function onClick(e:MouseEvent):void
		{
			dispatchEvent(new VideoLoadEvent(VideoLoadEvent.LOAD_VIDEO, _url));
		}
		
	}
}