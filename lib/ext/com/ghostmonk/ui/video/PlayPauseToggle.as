/**
 * Written by:Nicholas Hillier Nov. 18 2008 
 * @param play - movieclip composited into a MovieClipButton
 * @param pause - movieclip composited into a MovieClipButton
 * @param showPlay - default true: Shows play if true, pause if false
 * 
 * This toggles and dispatches events for pausing and playing video
 * 
*/
package com.ghostmonk.ui.video
{
	import com.ghostmonk.events.video.VideoControlEvent;
	import com.ghostmonk.ui.MovieClipButton;
	
	import flash.display.*;
	import flash.events.*;
	
	[Event(name="play", type="com.ghostmonk.events.VideoControlEvent")]
	[Event(name="pause", type="com.ghostmonk.events.VideoControlEvent")]
	
	public class PlayPauseToggle extends Sprite
	{
		private var _playButton:MovieClipButton;
		private var _pauseButton:MovieClipButton;
			
		public function PlayPauseToggle(play:MovieClip, pause:MovieClip, showPlay:Boolean = true)
		{
			_playButton = new MovieClipButton(play);
			_pauseButton = new MovieClipButton(pause);
			
			_playButton.addEventListener(MouseEvent.CLICK, onPlay);
			_pauseButton.addEventListener(MouseEvent.CLICK, onPause);
			
			if(showPlay)
			{
				addChild(_playButton);
			}
			else
			{
				addChild(_pauseButton);
			}
		}
		
		/**
		 * 
		 * @param clip0 - Clip will be disabled and removed
		 * @param clip1 - Clip will be enabled and added to the display
		 * 
		 */		
		private function setView(clip0:MovieClipButton, clip1:MovieClipButton):void
		{
			clip0.disable();
			clip1.enable();
			addChild(clip1);
		}
		
		private function onPlay(e:Event):void
		{
			setView(_playButton, _pauseButton);
			dispatchEvent(new VideoControlEvent(VideoControlEvent.PLAY));
		}
		
		private function onPause(e:Event):void
		{
			setView(_pauseButton, _playButton);
			dispatchEvent(new VideoControlEvent(VideoControlEvent.PAUSE));
		}
		
	}
}