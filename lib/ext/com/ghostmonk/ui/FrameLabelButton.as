/**
 * Written By: Nicholas Hillier, Dec 20, 2008
 * simple movieClip button allowing animation tweens to be constructed in the Flash IDE 
 */
package com.ghostmonk.ui
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	
	[Event(type="flash.events.MouseEvent", name="rollOver")]
	[Event(type="flash.events.MouseEvent", name="rollOut")]
	[Event(type="flash.events.MouseEvent", name="mouseDown")]
	[Event(type="flash.events.MouseEvent", name="mouseUp")]
	
	public class FrameLabelButton extends EventDispatcher
	{
		private var _clip:MovieClip;
		private var _mouseOver:int;
		private var _mouseDown:int;
		private var _callBack:Function;
		
		public function get view():MovieClip
		{
			return _clip;
		}
		
		/**
		 * 
		 * This class composes a movieClip made in the flash IDE with functionality of an interactive button
		 * using the mouseEvents rollOver, rollOut, mouseDown and mouseUp.
		 * 
		 * Tweener cause the clip to play, and the clip must have three labels "mouseOut", "mouseOver" and "mouseOut" to operate properly
		 * 
		 * @param clip 			_ movieClip created in the Flash IDE with three labels: 
		 * 							"rollOut" = destination for rollOut 
		 * 							"rollover" = destination for rollOver/mouseUp
		 * 							"mouseDown" = destination for mouseDown
		 * @param clickCallBack - function called when button clicked
		 * 
		 */
		public function FrameLabelButton(clip:MovieClip, clickCallBack:Function)
		{
			_clip = clip;
			_callBack = clickCallBack;
			setFrameDestinations();
			enable();
			_clip.mouseChildren = false;
		}
		
		public function enable():void
		{
			_clip.buttonMode = true;
			_clip.mouseEnabled = true;
			_clip.addEventListener(MouseEvent.ROLL_OVER, onRollover);
			_clip.addEventListener(MouseEvent.ROLL_OUT, onRollout);
			_clip.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_clip.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			_clip.addEventListener(MouseEvent.CLICK, onClick);
		}
		
		public function disable():void
		{
			_clip.buttonMode = false;
			_clip.mouseEnabled = false;
			_clip.removeEventListener(MouseEvent.ROLL_OVER, onRollover);
			_clip.removeEventListener(MouseEvent.ROLL_OUT, onRollout);
			_clip.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			_clip.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			_clip.removeEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function setFrameDestinations():void
		{
			_clip.gotoAndStop("rollOver");
			_mouseOver = _clip.currentFrame;
			_clip.gotoAndStop("mouseDown");
			_mouseDown = _clip.currentFrame;
			_clip.gotoAndStop("rollOut");
		}
		
		private function onRollover(e:MouseEvent):void  
		{ 	
			tweenTo(_mouseOver);
			dispatchEvent(e);	
		}
		
		private function onRollout(e:MouseEvent):void 	
		{ 	
			tweenTo(1);
			dispatchEvent(e); 			
		}
		
		private function onMouseDown(e:MouseEvent):void 
		{ 	
			tweenTo(_mouseDown);
			dispatchEvent(e);	
		}
		
		private function onMouseUp(e:MouseEvent):void   
		{ 	
			tweenTo(_mouseOver);
			dispatchEvent(e);	
		}
		
		private function onClick(e:MouseEvent):void
		{
			_callBack(e);
		}
		
		private function tweenTo(destFrame:int):void
		{
			Tweener.removeTweens(_clip);
			//Find the difference between current frame and destination frame, make it positive and divide by frameRate
			var transTime:Number = Math.abs((destFrame - _clip.currentFrame))/_clip.stage.frameRate;
			Tweener.addTween(_clip, {_frame:destFrame, time:transTime, transition:Equations.easeNone});
		}
		
	}
}