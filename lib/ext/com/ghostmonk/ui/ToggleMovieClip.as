/** 
 * Written by Nicholas hillier: Nov 29th 2008
 * You must have The tweener library installed to use this class
 * and you must set DisplayShortcuts.init() in the main class
 * @param - MovieClip.... onClick the movieClip is tweened back and forth from totalframes to the first frame
 * A toggle event is dispatched with the Boolean isOn.. indicating the state of the button
 * when the movieclip is on frame 1, the clip is considered off... 
 * when the clip is on totalFrames the clip is considerd on 
 * The button is initialized in an off state
*/
package com.ghostmonk.ui
{
	import caurina.transitions.*;
	
	import com.ghostmonk.events.ToggleEvent;
	
	import flash.display.*;
	import flash.events.MouseEvent;
	
	[Event(name="toggle", type="com.ghostmonk.events.ToggleEvent")]

	public class ToggleMovieClip extends Sprite
	{
		private var _clip:MovieClip;
		private var _isOn:Boolean;
		
		public function get isOn():Boolean
		{
			return _isOn;
		}
		
		public function ToggleMovieClip(clip:MovieClip)
		{
			_clip = clip;
			_isOn = false;
			addChild(_clip);
			mouseChildren = false;
			buttonMode = true;
			addEventListener(MouseEvent.CLICK, onClick);	
		}
		
		private function onClick(e:MouseEvent):void
		{
			_isOn = !_isOn;
			var destFrame:int = _isOn ? _clip.totalFrames : 1;
			//Find difference of current frame and destination frame and divide by frameRate
			var time:Number = Math.abs(_clip.currentFrame - destFrame)/stage.frameRate;
			Tweener.addTween(_clip, {_frame:destFrame, time:time, transition:Equations.easeNone});
			dispatchEvent(new ToggleEvent(ToggleEvent.TOGGLE, _isOn));
		}
		
	}
}