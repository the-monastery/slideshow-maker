package com.ghostmonk.ui
{
	import flash.text.TextField;

	public class TimeDisplay
	{
		private var _display:TextField;
		private var _currentTotalTime:String;
		
		public function get view():TextField
		{
			return _display;
		}
		
		public function TimeDisplay(textField:TextField)
		{
			_display = textField;
			reset();
		}
		
		public function reset():void
		{
			_display.text = "00:00:00";
		}
		
		public function setTime(seconds:Number):void
		{
			_display.text = calculateTime(seconds);
		}
		
		public function setTotalTime(seconds:Number):void
		{
			_currentTotalTime = calculateTime(seconds);
			_display.text = "00:00:00";
		}
		
		private function calculateTime(time:Number):String
		{
			var minutes:Number = Math.floor(time/60);
			var displayMins:String = minutes < 10 ? "0" + minutes.toString() : minutes.toString();
			
			var seconds:Number = Math.floor(time - (minutes*60));
			var displaySecs:String = seconds < 10 ? "0" + seconds.toString() : seconds.toString();
			
			var milliseconds:Number = Math.floor(100*(time - Math.floor(time)));
			var displayMilli:String = milliseconds < 10 ? "0" + milliseconds.toString() : milliseconds.toString();
			return displayMins + ":" + displaySecs + ":" + displayMilli;
		}
		
	}
}