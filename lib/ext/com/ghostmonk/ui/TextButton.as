/**
 * Written By: Nicholas Hillier Dec 20, 2008
 * Creates an interactive button with rollover and rollout states
 * Embeds fonts into a font field 
 */
package com.ghostmonk.ui
{
	import com.ghostmonk.text.EmbeddedText;
	
	import flash.display.*;
	import flash.events.MouseEvent;

	public class TextButton extends Sprite
	{
		private var _textColor:uint;
		private var _baseColor:uint;
		
		private var _text:EmbeddedText;
		private var _background:Bitmap;
		
		private var _width:Number;
		
		override public function get width():Number
		{
			return _width;
		}
		
		/**
		 * 
		 * @param font		- Name of font used for label display, font must be available for embed
		 * @param textSize 	- Size of text
		 * @param label		- Display Label
		 * @param textColor	- Color of text on rollOut, color of background on rollOver
		 * @param baseColor	- Color of background on rollOut, color of text on rollOver
		 * 
		 */
		public function TextButton(font:String, textSize:Number, label:String, textColor:uint, baseColor:uint)
		{
			_text = new EmbeddedText(font, textSize, textColor, true);
			_text.text = label;
			
			_textColor = textColor;
			_baseColor = baseColor;
			_width = _text.width;
			
			_background = new Bitmap(new BitmapData(width, _text.height, true, 0));
			addChild(_background);
			addChild(_text); 
			
			buttonMode = true;
			mouseChildren = false;
			
			enable();
		}
		
		public function enable():void
		{
			addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		public function disable():void
		{
			removeEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
			removeEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
		}
		
		private function onMouseOver(e:MouseEvent):void
		{
			if(_baseColor)
			{
				_text.color = _baseColor;
				_background.bitmapData = new BitmapData(width, _text.height, false, _textColor);
			}	
		}
		
		protected function onMouseOut(e:MouseEvent):void
		{
			_text.color = _textColor;
			_background.bitmapData.dispose();
			_background.bitmapData = new BitmapData(width, _text.height, true, 0);
		}
		
	}
}