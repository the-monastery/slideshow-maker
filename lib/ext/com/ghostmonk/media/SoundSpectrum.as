package com.ghostmonk.media
{
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.shapes.InnerShadowRectangle;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.SoundMixer;
	import flash.utils.ByteArray;

	public class SoundSpectrum extends Sprite
	{
		private var _bg:InnerShadowRectangle;
		private var _splash:Sprite;
		
		public function SoundSpectrum()
		{
			_splash = new Sprite();
			addEventListener(Event.ADDED_TO_STAGE, drawBG);
			alpha = 0;
		}
		
		public function start():void
		{
			addEventListener(Event.ENTER_FRAME, compute);
		}
		
		public function stop():void
		{
			removeEventListener(Event.ENTER_FRAME, compute);
		}
		
		public function show():void
		{
			start();
			Tweener.addTween(this, {alpha:1, time:0.5, transition:Equations.easeNone});
		}
		
		public function hide():void
		{
			Tweener.addTween(this, {alpha:0, time:0.5, transition:Equations.easeNone, onComplete:stop});
		}
		
		public function compute(e:Event):void
		{
			var bytes:ByteArray = new ByteArray();
			const PLOT_HEIGHT:int = parent.height/2;
            const CHANNEL_LENGTH:int = 256;
            
			SoundMixer.computeSpectrum(bytes);
			
			var g:Graphics = _splash.graphics;
            
            g.clear();
       
            g.lineStyle(0, 0x8060B2, 0);
            g.beginFill(0x8060B2);
            g.moveTo(0, PLOT_HEIGHT);
            
            var n:Number = 0;
            
            for (var i:int = 0; i < CHANNEL_LENGTH; i++) 
            {
                n = (bytes.readFloat() * PLOT_HEIGHT);
                g.lineTo(i * 2, PLOT_HEIGHT - n);
            }
			
			g.lineTo(CHANNEL_LENGTH * 2, PLOT_HEIGHT);
            g.endFill();
 
            g.lineStyle(0, 0x12FFCF, 0);
            g.beginFill(0x12FFCF, 0.5);
            g.moveTo(CHANNEL_LENGTH * 2, PLOT_HEIGHT);
			
			for (i = CHANNEL_LENGTH; i > 0; i--) 
			{
                n = (bytes.readFloat() * PLOT_HEIGHT);
                g.lineTo(i * 2, PLOT_HEIGHT - n);
            }
  
			g.lineTo(0, PLOT_HEIGHT);
            g.endFill();
		}
		
		
		private function drawBG(e:Event):void
		{
			_bg = new InnerShadowRectangle(0x000000, parent.width, parent.height);
			addChild(_bg);
			addChild(_splash);
			removeEventListener(Event.ADDED_TO_STAGE, drawBG);
			/* graphics.beginFill(0x000000);
			graphics.drawRect(0,0,parent.width,parent.height);
			graphics.endFill(); */
		}

	}
}