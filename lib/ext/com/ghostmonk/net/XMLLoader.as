/**
 *Written by Nicholas Hillier Dec 13 2008
 *Hides the basic functions of a single request for an XML file
 * 
 * @params url:String
 * @params callBack:Function : this function should expect to received XML in its constructure 
*/
package com.ghostmonk.net
{
	import flash.events.*;
	import flash.net.*;

	public class XMLLoader extends URLLoader
	{
		private var _callBack:Function;
		
		public function XMLLoader(url:String, callBack:Function)
		{
			_callBack = callBack;
			addEventListener(Event.COMPLETE, onXMLLoadComplete);
			addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			super(new URLRequest(url));
		}
		
		private function onXMLLoadComplete(e:Event):void
		{
			removeEventListener(Event.COMPLETE, onXMLLoadComplete);
			removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
			
			_callBack(XML(data));
			null
			delete this;
		}
		
		private function onIOError(e:IOErrorEvent):void
		{
			trace(e.text);
		}
		
	}
}