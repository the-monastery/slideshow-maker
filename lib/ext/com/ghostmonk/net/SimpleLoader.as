package com.ghostmonk.net
{
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;

	public class SimpleLoader extends Loader
	{
		private var _completeCall:Function;
		private var _errorCall:Function;
		private var _index:int;
		
		public function SimpleLoader(completeCall:Function, errorCall:Function = null, index:int = -1)
		{
			_index = index;
			_completeCall = completeCall;
			_errorCall = errorCall;
			contentLoaderInfo.addEventListener(Event.COMPLETE, onComplete);
			contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, onError);
		}
		
		public function nullify():void
		{
			contentLoaderInfo.removeEventListener(Event.COMPLETE, onComplete);
			contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, onError);
			delete this;
		}
		
		private function onComplete(e:Event):void
		{
			_index > -1 ? _completeCall(contentLoaderInfo.content, _index) : _completeCall(contentLoaderInfo.content);
			nullify();
		}
		
		private function onError(e:IOErrorEvent):void
		{
			if(_errorCall != null)
				_index > -1 ? _errorCall(_index) : _errorCall();
			nullify();
		}
		
	}
}