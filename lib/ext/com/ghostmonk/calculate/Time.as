/**
* Written by Nicholas Hillier Dec 18 2008
 * Planned on putting som static utility functions in this calculate package
 * However,  they need more thought 
*/
package com.ghostmonk.calculate
{
	public class Time
	{
		public static function minutes(seconds:Number):Number
		{
			return seconds/60;
		}
		
		public static function seconds(milliseconds:Number):Number
		{
			return milliseconds*100/60;
		}

	}
}