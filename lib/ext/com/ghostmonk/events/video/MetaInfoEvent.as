/**
 * Written by: Nicholas Hillier Nov 30 2008
 * Meant to be used when onMetaData is fired from a loaded Video object (flv)
 * @param type - metaInfoReady
 * @param metaData - VideoMetaData data object
*/
package com.ghostmonk.events.video
{
	import com.ghostmonk.model.VideoMetaData;
	
	import flash.events.Event;

	public class MetaInfoEvent extends Event
	{
		public static const META_INFO_READY:String = "metaInfoReady";
		
		private var _metaData:VideoMetaData;
		
		public function get metaData():VideoMetaData
		{
			return _metaData;
		}
		
		public function MetaInfoEvent(type:String, metaData:VideoMetaData, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_metaData = metaData;
		}
		
		override public function clone():Event
		{
			return new MetaInfoEvent(type, metaData);
		}
		
	}
}