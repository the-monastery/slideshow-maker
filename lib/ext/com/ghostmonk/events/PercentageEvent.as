/**
* Written By: Nicholas Hillier Nov 22, 2008
 * This event is used to broadast a percentage loaded number
*/
package com.ghostmonk.events
{
	import flash.events.Event;

	public class PercentageEvent extends Event
	{
		public static const CHANGE:String = "change";
		
		private var _percent:Number;
		
		public function get percent():Number
		{
			return _percent;
		}
		
		public function PercentageEvent(type:String, value:Number, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_percent = value;
		}
		
		override public function clone():Event
		{
			return new PercentageEvent(type, percent);
		}
		
	}
}