package com.ghostmonk.organic.data
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	
	public class PixelValuesTest extends Sprite
	{
		private const WIDTH:Number = 255;
		private const HEIGHT:Number = 1;
		
		private var _bitmapData:BitmapData = new BitmapData(WIDTH,HEIGHT,false);
		private var _bitmap:Bitmap = new Bitmap(_bitmapData);
		
		public function PixelValuesTest()
		{
			var matrix:Matrix = new Matrix();
			matrix.createGradientBox(WIDTH,HEIGHT);
			var gradient:Sprite = new Sprite();
			gradient.graphics.beginGradientFill(GradientType.LINEAR, [0x000000,0xffffff],[1,1],[0,255],matrix);
			gradient.graphics.drawRect(0,0,WIDTH,HEIGHT);
			gradient.graphics.endFill();
			
			_bitmapData.draw(gradient);
			addChild(_bitmap);
			for(var i:Number = 0; i < WIDTH; i++)
			{
				trace(Math.floor(_bitmapData.getPixel(i,0)/16711422*255));
			}
			
		}

	}
}