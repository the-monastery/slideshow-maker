/**
 * Written By: Nicholas Hillier Nov 10 2008
 * 
 * NEEDS REFACTORING
 * 
 * Constants used in error reporting for form validation. 
*/
package com.ghostmonk.form
{
	import flash.filters.GlowFilter;
	
	public class FormConstants
	{
		public static var IS_LIVE:Boolean = true;
		public static var FORM_GLOW:GlowFilter = new GlowFilter(0xE2D097, 0.7, 10, 10, 2, 4);
		public static var NAME_ERROR:String = "* Please include your name";
		public static var EMAIL_MISSING:String = "* Please include your email";
		public static var EMAIL_ERROR:String = "* There is an error in format of your email";
		public static var SERVER_SCRIPT_URL:String = IS_LIVE ? "submitEmailCollectionForm.php" : "http://localhost/ccap/forms/signUpForm.php";
	}
}