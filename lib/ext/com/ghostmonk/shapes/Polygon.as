/**
 * Written By Nicholas Hillier Dec 30, 2008 
 */
package com.ghostmonk.shapes
{
	import flash.display.Shape;

	public class Polygon extends Shape
	{
		private var _origColor:uint;
		
		public function get origColor():uint
		{
			return _origColor;
		}
		/**
		 * Simple class that builds a variable sided polygon...
         * Note that the registration point is located exactly in the middle of the shape
		 * 
		 * @param sides
		 * @param color
		 * @param width
		 * 
		 */
		public function Polygon(sides:int, width:Number, color:uint, angle:Number = 0, xScale:Number = 1, yScale:Number = 1)
		{
			var radius:Number = width/2;
			var rad:Number = (360/sides) * Math.PI/180;
			
			_origColor = color;
			
			graphics.beginFill(color,1);
					 
			for(var i:int = 1; i <= sides; i++)
			{
				i == 1 
				? graphics.moveTo(Math.cos(rad*i)*radius,Math.sin(rad*i)*radius)
				: graphics.lineTo(Math.cos(rad*i)*radius,Math.sin(rad*i)*radius);	
			}
			
			graphics.endFill();
			
			rotation = angle;
			scaleX = xScale;
			scaleY = yScale;
		} 
		
	}
}