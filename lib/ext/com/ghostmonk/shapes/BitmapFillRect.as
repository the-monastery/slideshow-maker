package com.ghostmonk.shapes
{
	import flash.display.BitmapData;
	import flash.display.Sprite;

	public class BitmapFillRect extends Sprite
	{
		private var _data:BitmapData;
		
		public function BitmapFillRect(data:BitmapData)
		{
			_data = data;
		}
		
		public function make(width:Number, height:Number):void
		{
			graphics.clear();
			graphics.beginBitmapFill(_data);
			graphics.drawRect(0,0,width,height);
			graphics.endFill();
		}
		
	}
}