package com.ghostmonk.shapes
{
	import flash.display.Sprite;

	public class EasyRectangle extends Sprite
	{
		private var _color:uint;
		private var _alpha:Number;
		
		public function EasyRectangle(color:uint, alpha:Number)
		{
			_color = color;
			_alpha = alpha;
		}
		
		public function create(width:Number, height:Number):void
		{
			graphics.beginFill(_color,_alpha);
			graphics.drawRect(0,0,width,height);
			graphics.endFill();
		}
		
	}
}