package com.ghostmonk.shapes
{
	import flash.display.Sprite;

	public class Circle extends Sprite
	{
		private var _color:int;
		private var _radius:Number;
		
		public function setColor(color:int):void
		{
			_color = color;
			redraw();	
		}
		
		public function Circle(color:int,radius:Number)
		{
			_color = color;
			_radius = radius;
			redraw();	
		}
		
		public function redraw():void
		{
			graphics.clear();
			graphics.beginFill(_color, 1);
			graphics.drawCircle(-_radius,-_radius,_radius);
			graphics.endFill();	
		}
	}
}