package com.ghostmonk.shapes
{
	import flash.display.Sprite;
	import flash.filters.BlurFilter;

	public class BlurredRectangle extends Sprite
	{
		public function BlurredRectangle(color:uint, width:int, height:int)
		{
			graphics.beginFill(color);
			graphics.drawRect(0,0,width,height);
			graphics.endFill();
			filters = [new BlurFilter()];
		}
		
	}
}